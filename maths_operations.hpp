#ifndef UTILITY_FUNC_HPP
#define UTILITY_FUNC_HPP
#include <math.h>
#include <random>


namespace
{
	std::default_random_engine createRandomEngine()
	{
		auto seed = static_cast<unsigned long>(std::time(nullptr));
		return std::default_random_engine(seed);
	}

	auto RandomEngine = createRandomEngine();
}

class Utility {
public:

	static float square(float x) { return x * x; }


    //angle in radians, rotating around z axis in 2D
    static void rotateVector(sf::Vector2f& vec_to_rotate, float angle)
    {
        /**
         * matrix: cos -sin * x
         *         sin  cos   y
         * 
         */
        float x = vec_to_rotate.x;
        float y = vec_to_rotate.y;
        vec_to_rotate.x = std::cos(angle) * x - std::sin(angle) * y;
        vec_to_rotate.y = std::sin(angle) * x + std::cos(angle) * y;
    }

	static void projectPointsOnAxis(const std::vector<sf::Vector2f>& points,
		const sf::Vector2f& axis,
		float& min_proj, float& max_proj)
	{
		float current_proj = 0.f;
		min_proj = INFINITY, max_proj = -INFINITY;
		
		for(auto& point : points)
		{
			current_proj = Utility::dotProd(point, axis);
			min_proj = std::min(min_proj, current_proj);
			max_proj = std::max(max_proj, current_proj);
		}

	}

	static sf::Vector2f getRotateVector(const sf::Vector2f& vec_to_rotate, float angle)
	{
        return sf::Vector2f(std::cos(angle) * vec_to_rotate.x - std::sin(angle) * vec_to_rotate.y,
		 std::sin(angle) * vec_to_rotate.x + std::cos(angle) * vec_to_rotate.y);
	}

	static sf::Vector2f normalize(const sf::Vector2f& v)
	{
		return v / (float)length(v);
	}

	static float dotProd(const sf::Vector2f& a, const sf::Vector2f& b)
	{
		return a.x * b.x + a.y * b.y;
	}

	static sf::Vector2f Cross2DScalar( const sf::Vector2f& v, float a )
	{
		return sf::Vector2f(a * v.y, -a * v.x);
	}

	static sf::Vector2f Cross2DScalar(float a, const sf::Vector2f& v )
	{
		return sf::Vector2f(-a * v.y, a * v.x);
	}

	static float crossProduct2D(const sf::Vector2f& a, const sf::Vector2f& b)
	{
		/**
		 * reminder:  a x b = (a1.u_x + a2.u_y) x (b1.u_x + b2.u_y)
		 * 					= a1b2 - a2b1 .u_z
		 * 
		 */
		return a.x * b.y - a.y * b.x;
	}

	static float triangleArea(float a, float b, float c)
	{
		float s = a + b + c;
		s *= 0.5f;

		return std::sqrt(s * (s - a) * (s - b) * (s - c));
	}

	static float length(const sf::Vector2f& vec)
	{
		return std::sqrt(square(vec.x) + square(vec.y));
	}

	static float getDistance(const sf::Vector2f& a, const sf::Vector2f& b)
	{
		return length(b - a);
	}

	static int randomInt(int exclusiveMax)
	{
		std::uniform_int_distribution<> distr(0, exclusiveMax - 1);
		return distr(RandomEngine);
	}

	static int randomInt(int min, int max)
	{
		std::uniform_int_distribution<> distr(min, max);
		return distr(RandomEngine);
	}

	static float randomFloat(float Max)
	{
		std::uniform_real_distribution<> distr(0.f, Max);
		return float(distr(RandomEngine));
	}

	static double randomFloat(double min, double max)
	{
		std::uniform_real_distribution<> distr(min, max);
		return distr(RandomEngine);
	}
};

#endif //UTILITY_FUNC_H