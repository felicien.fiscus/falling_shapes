#include "SFML/Graphics.hpp"
#include "../engine/polygon.hpp"
#include "../engine/physical_circle.hpp"


int main()
{
    sf::RenderWindow win{sf::VideoMode(600, 400), "test", sf::Style::Close};

/*     Polygon poly(50.f, 5);
    poly.fMass = 0.5f;
    poly.setPosition(300, 200); */

    Polygon poly3;
        poly3.setPolygon({
        sf::Vector2f(150, 15),
        sf::Vector2f(100, 30),
        sf::Vector2f(80, -50),
        sf::Vector2f(100, -100)});
        poly3.setPosition(300, 200);
        poly3.setColor(sf::Color::Blue);
        poly3.fAngularVelocity = 0.f;
        poly3.setMass(0.5f);
        poly3.fLinearVelocity.x = 0.f;
        poly3.fInitialRestCoeff = .5f;
        poly3.fRestitutionCoeff = poly3.fInitialRestCoeff;
        poly3.fGravitySensitive = true;
        poly3.rotate(50.f);


    Polygon circle(50.f, 4);

    //PhysicalCircle circle(20.f);
    circle.setColor(sf::Color::Blue);
    circle.setPosition(100.f, 50.f);
    float speed = .05f;
    OverlapData overlap_data;

    while(win.isOpen())
    {
        sf::Event e;
        while(win.pollEvent(e))
        {
            switch (e.type)
            {
            case sf::Event::Closed:
                win.close();
                break;
            case sf::Event::KeyPressed:
                if(e.key.code == sf::Keyboard::Space)
                {
                    circle.setColor(sf::Color::Blue);
                }

                break;

            default:
                break;
            }
        }

        circle.fLinearVelocity.x = circle.fLinearVelocity.y = 0.f;
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            circle.fLinearVelocity.x = -1.f;
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            circle.fLinearVelocity.x = 1.f;
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            circle.fLinearVelocity.y = 1.f;
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            circle.fLinearVelocity.y = -1.f;
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
        {
            circle.rotate(-0.01f);
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        {
            circle.rotate(0.01f);
        }
        
        if(fabs(circle.fLinearVelocity.x) > 1e-7 && fabs(circle.fLinearVelocity.y) > 1e-7)
        {
            circle.fLinearVelocity /= Utility::length(circle.fLinearVelocity);
        }


        circle.move(speed * circle.fLinearVelocity);

        circle.setColor(sf::Color::Blue);

        if(poly3.doesCollide(circle, overlap_data))
        {
            circle.setColor(sf::Color::Red);
        }

        win.clear();

        win.draw(poly3);
        win.draw(circle);

        win.display();
    }
    return 0;
}