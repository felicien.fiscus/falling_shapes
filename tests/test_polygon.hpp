#ifndef TEST_POLYGON_HPP
#define TEST_POLYGON_HPP
#include <cxxtest/TestSuite.h>
#include "../engine/polygon.hpp"
#define TEST_EPSILON 1e-4   

class Test_Polygon : public CxxTest::TestSuite
{
public:
    void testInertiaSquare()
    {
        TS_TRACE("Begining square inertia computation");

        Polygon square(10.f, 4U);
        square.setMass(1.f);
        float a = square.getSideLength();
        float inertia = (square.getMass() / square.getArea()) * std::pow(a, 4) / 6.f; 
        
        TS_ASSERT_DELTA(square.getInertiaMomentum(), inertia, TEST_EPSILON);
        
        
        TS_TRACE("End square inertia computation");
    }

    void testInertiaRectangle()
    {
        TS_TRACE("Begining rect inertia computation");

        Polygon rect;
        rect.setPolygon({sf::Vector2f(0.f, 0.f),sf::Vector2f(50.f, 0.f),sf::Vector2f(50.f,10.f),sf::Vector2f(0.f, 10.f)});
        rect.computeCentroid();
        rect.setMass(1.f);
        float b = 50.f, h = 10.f;
        float area = rect.getArea();
        float inertia = (b * h*h*h + h*b*b*b)/(12.f * area);
        TS_ASSERT_DELTA(rect.getInertiaMomentum(), inertia, TEST_EPSILON);
        TS_TRACE("End rect inertia computation");
    }
};

#endif //TEST_POLYGON_HPP