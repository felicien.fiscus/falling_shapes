#include "physics_engine.hpp"
#include "../maths_operations.hpp"
#define MY_EPS 1e-7f
#define SPEED_LIMIT 1e14f
#define CONVERSION_FACTOR 1e-2f
#define INVERSE_CONVERSION_FACTOR 1e2f

namespace
{

    //formulas are based on this incredible lecture https://perso.liris.cnrs.fr/nicolas.pronost/UUCourses/GamePhysics/lectures/lecture%207%20Collision%20Resolution.pdf
    void add_Impulses(PhysicalObject* a, PhysicalObject* b, OverlapData data)
    {
        float I_1 = a->getInverseInertia() * Utility::square(INVERSE_CONVERSION_FACTOR);  //multiplying by the conversion factor to match the 1pixel -> 1cm units
        float I_2 = b->getInverseInertia() * Utility::square(INVERSE_CONVERSION_FACTOR);
        float Im1 = a->getInverseMass();
        float Im2 = b->getInverseMass();

        
        
        float contact_factor = 1.f / (float)data.fContactPoints.size;
        for(unsigned int i = 0;i<data.fContactPoints.size;++i)
        {
            //need to convert cm to m
            sf::Vector2f r_a = (data.fContactPoints[i] - a->getPosition()) * CONVERSION_FACTOR; //multiplying by conversion factor to match the 1pixel -> 1cm units
            sf::Vector2f r_b = (data.fContactPoints[i] - b->getPosition()) * CONVERSION_FACTOR;

            float raCrossN = Utility::crossProduct2D(r_a, data.fCollisionNormal);
            float rbCrossN = Utility::crossProduct2D(r_b, data.fCollisionNormal);
            sf::Vector2f v_r = a->fLinearVelocity + Utility::Cross2DScalar(a->fAngularVelocity, r_a) -
             b->fLinearVelocity - Utility::Cross2DScalar(b->fAngularVelocity, r_b);
            
            float vr_alongNormal = Utility::dotProd(v_r, data.fCollisionNormal);
            if(vr_alongNormal < 0.0)
                return;
                
            float angular_addition = I_1 * Utility::square(raCrossN)
                + I_2 * Utility::square(rbCrossN);

            //NOT SURE ABOUT THIS but it works-ish for now
            if(vr_alongNormal < 1.f)
            {
                a->fRestitutionCoeff = 0.f;
                b->fRestitutionCoeff = 0.f;
            }
            else if(vr_alongNormal > 2.f)
            {
                a->fRestitutionCoeff = a->fInitialRestCoeff;
                b->fRestitutionCoeff = b->fInitialRestCoeff;
            }

            float e = 0.5f * (a->fRestitutionCoeff + b->fRestitutionCoeff);

            float j = (1.f + e) * (vr_alongNormal)
                / (Im1 + Im2 + angular_addition);
            j *= contact_factor;
            j = std::max(j, 0.f);

            
            if(Im1 > MY_EPS)
            {
                a->fLinearVelocity -= Im1 * (j * data.fCollisionNormal);
                a->fAngularVelocity -= I_1 * Utility::crossProduct2D(r_a, j * data.fCollisionNormal);
            }
            if(Im2 > MY_EPS)
            {
                b->fLinearVelocity += Im2 * (j * data.fCollisionNormal);
                b->fAngularVelocity += I_2 * Utility::crossProduct2D(r_b, j * data.fCollisionNormal);
            }
 
            /****TANGENT IMPULSE*****/

            v_r = a->fLinearVelocity + Utility::Cross2DScalar(a->fAngularVelocity, r_a) -
             b->fLinearVelocity - Utility::Cross2DScalar(b->fAngularVelocity, r_b);


            sf::Vector2f t = v_r - Utility::dotProd(v_r, data.fCollisionNormal) * data.fCollisionNormal;
            t = Utility::normalize(t);
            float raCrossT = Utility::crossProduct2D(r_a, t);
            float rbCrossT = Utility::crossProduct2D(r_b, t);

            angular_addition = I_1 * Utility::square(raCrossT)
                + I_2 * Utility::square(rbCrossT);

            
            sf::Vector2f tangent_impulse;
            
            float j_t = Utility::dotProd(v_r, t) / (Im1 + Im2 + angular_addition);
            j_t *= contact_factor;

            if(fabs(j_t) <= MY_EPS)
                return;
            
            float mu_s = sqrt(Utility::square(a->fStaticFriction) + Utility::square(b->fStaticFriction));   //static friction
            float mu_k = sqrt(Utility::square(a->fDynamicFriction) + Utility::square(b->fDynamicFriction)); //kinetic friction
            //TODO: little bug with static friction not being used when relative velocity is low (especially with squares for whatever reason)
            if(fabs(j_t) < mu_s * j)
            {
                tangent_impulse = j_t * t;
            }
            else{
                tangent_impulse = mu_k * j * t;
            }

            if(Im1 > MY_EPS)
            {
                a->fLinearVelocity -= Im1 * (tangent_impulse);
                a->fAngularVelocity -= I_1 * Utility::crossProduct2D(r_a, tangent_impulse);
            }
            if(Im2 > MY_EPS)
            {
                b->fLinearVelocity += Im2 * (tangent_impulse);
                b->fAngularVelocity += I_2 * Utility::crossProduct2D(r_b, tangent_impulse);
            }

        }
    }


    //formulas are based on this incredible lecture https://perso.liris.cnrs.fr/nicolas.pronost/UUCourses/GamePhysics/lectures/lecture%207%20Collision%20Resolution.pdf
    void modify_velocities(PhysicalObject* a, PhysicalObject* b, OverlapData data)
    {
        add_Impulses(a, b, data);
    }
}

PhysicsEngine::PhysicsEngine(const sf::RenderWindow& window, unsigned int sub_steps)
    :fObjects(), fGravitySensitiveObjects(), fInitialValues(), fWindow(window), fSubsteps(sub_steps)
{
    
}

PhysicsEngine::~PhysicsEngine()
{
    for(auto* obj : fObjects)
    {
        if(obj)
            delete obj;
    }
}


std::vector<PhysicalObject*>::iterator PhysicsEngine::begin()
{
    return fObjects.begin();
}
std::vector<PhysicalObject*>::iterator PhysicsEngine::end()
{
    return fObjects.end();
}


std::list<PhysicalObject*>& PhysicsEngine::getNonStaticObjects()
{
    return fGravitySensitiveObjects;
}

void PhysicsEngine::addObject(PhysicalObject* obj)
{
    fObjects.push_back(obj);
    if(obj->fGravitySensitive)
        fGravitySensitiveObjects.push_back(obj);
    PhysicalObjectData data;
    data.fInitialAngularVelocity = obj->fAngularVelocity;
    data.fInitialLinearVelocity = obj->fLinearVelocity;
    data.fInitialPosition = obj->getPosition();
    data.fInitialRotation = obj->getRotation();
    data.fInitialRestitutionCoeff = obj->fRestitutionCoeff;
    data.fInitialMass = obj->getMass();
    fInitialValues.push_back(data);
}

void PhysicsEngine::removeObject(PhysicalObject *obj)
{
    fGravitySensitiveObjects.remove(obj);
    auto found = std::find(fObjects.begin(), fObjects.end(), obj);
    if(found != fObjects.end())
    {
        fObjects.erase(found);
    }
}

void PhysicsEngine::removeGravityOnObject(PhysicalObject* obj)
{
    obj->fGravitySensitive = false;
    fGravitySensitiveObjects.remove(obj);
}

void PhysicsEngine::addGravityOnObject(PhysicalObject* obj)
{
    if(!obj->fGravitySensitive)
    {
        obj->fGravitySensitive = true;
        fGravitySensitiveObjects.push_back(obj);
    }
}

void PhysicsEngine::handleEvent(const sf::Event& event)
{
    switch (event.type)
    {
    case sf::Event::KeyPressed:
        switch (event.key.code)
        {
        case sf::Keyboard::S:
            fStartStop = !fStartStop;
            if(!fStartStop)
            {
                for(unsigned int i = 0;i<fObjects.size();++i)
                {
                    fObjects[i]->fLinearVelocity = fInitialValues[i].fInitialLinearVelocity;
                    fObjects[i]->fAngularVelocity = fInitialValues[i].fInitialAngularVelocity;
                    fObjects[i]->setPosition(fInitialValues[i].fInitialPosition);
                    fObjects[i]->setRotation(fInitialValues[i].fInitialRotation);
                    fObjects[i]->fRestitutionCoeff = fInitialValues[i].fInitialRestitutionCoeff;
                    fObjects[i]->setMass(fInitialValues[i].fInitialMass);
                }
                fTimeSinceStart = sf::Time::Zero;
            }
            break;
        case sf::Keyboard::Right:
            break;
        case sf::Keyboard::Left:
            break;
        default:
            break;
        }
        break;
    
    default:
        break;
    }
}

void PhysicsEngine::update(sf::Time dt)
{
    if(fStartStop)
    {
        fTimeSinceStart += dt;
        
        sf::Time sub_dt = dt / (float)(fSubsteps);

        applyGravity();

        for(unsigned int step = 0; step < fSubsteps;++step)
        {
            fCollisionPairs.clear();

            integrateForces(sub_dt);
            computeSweptBounds(sub_dt);
            //the solving happens inside the collision detection functions
            computeCollisions();
            updateVelocities();
            updatePositions(sub_dt);    
        }

        clearForces();

        

    }
}

void PhysicsEngine::integrateForces(sf::Time dt)
{
    for(auto* obj : fObjects)
    {
        obj->intergrateForcesAndTorque(dt);
    }
}

void PhysicsEngine::clearForces()
{
    for(auto* obj : fObjects)
    {
        obj->clearForces();
    }
}

void PhysicsEngine::computeSweptBounds(sf::Time dt)
{
    for(auto* obj : fObjects)
    {
        obj->computeSweptBounds(obj->fLinearVelocity * dt.asSeconds());
    }
}

void PhysicsEngine::applyGravity()
{
    for(auto* obj : fGravitySensitiveObjects)
    {
        obj->applyForce(obj->getMass() * INVERSE_CONVERSION_FACTOR * sf::Vector2f(0.f, fGravityConstant)); //multiplying by conversion factor to match the 1pixel -> 1cm units   
    }
}

void PhysicsEngine::solveCollisions()
{
    for(auto& pair : fCollisionPairs)
    {
        if(pair.a->getMass() >= 0.f)
            pair.a->fLinearVelocity -= pair.data.fOverlapAmount * pair.data.fCollisionNormal;
        else if (pair.b->getMass() >= 0.f)
            pair.b->fLinearVelocity += pair.data.fOverlapAmount * pair.data.fCollisionNormal;
    }
}

void PhysicsEngine::computeCollisions()
{
    for(int i = 0;i<(int)fObjects.size();++i)
    {
        OverlapData overlap_data;
        for(int j = i + 1;j<(int)fObjects.size();++j)
        {
            
            if((fObjects[i]->getMass() >= 0.f || fObjects[j]->getMass() >= 0.f) && fObjects[i]->doesCollide(*fObjects[j], overlap_data))
            {
                fCollisionPairs.push_front(CollisionPair(fObjects[i], fObjects[j], overlap_data));
            }
            
        }
    }

}

void PhysicsEngine::updatePositions(sf::Time dt)
{
    for(auto* obj : fObjects)
    {
        if(Utility::dotProd(obj->fLinearVelocity, obj->fLinearVelocity) < SPEED_LIMIT)
        {
            obj->move(obj->fLinearVelocity * dt.asSeconds());
            obj->rotate(obj->fAngularVelocity * dt.asSeconds());
        }
        else
        {
            obj->fLinearVelocity.x = obj->fLinearVelocity.y = 0.f;
            obj->fAngularVelocity = 0.f;
        }
    }
}
void PhysicsEngine::updateVelocities()
{
    for(auto& pair : fCollisionPairs)
    {
        modify_velocities(pair.a, pair.b, pair.data);
    }
}

void PhysicsEngine::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    for(auto* obj : fObjects)
    {

        target.draw(*obj, states);
    }
}