#include "polygon.hpp"
#include "physical_circle.hpp"
#include <list>
#define contact_epsilon 1e-2f

//Polygon with 0 points
//you can create a random polygon manually using the addPoint method
Polygon::Polygon()
    :PhysicalObject(),
      fSweptBounds(sf::PrimitiveType::LineStrip), fPoints(sf::PrimitiveType::LineStrip), farthestDistanceFromCentroid(-INFINITY),
      fContactPointVisual(sf::PrimitiveType::Lines), fMainFeatures(sf::PrimitiveType::Lines)
{
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Green));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Green));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Red));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Red));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color(255, 128, 0, 255)));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color(255, 128, 0, 255)));

    fMainFeatures.append(sf::Vertex(sf::Vector2f(), sf::Color::Magenta));
    fMainFeatures.append(sf::Vertex(sf::Vector2f(), sf::Color::Magenta));
    fMainFeatures.append(sf::Vertex(sf::Vector2f(), sf::Color::Cyan));
    fMainFeatures.append(sf::Vertex(sf::Vector2f(), sf::Color::Cyan));

    fColor = sf::Color::Blue;
}
//it will create a regular polygon with a certain number of points (3 -> triangle, 4 -> square, 5 -> pentagon, ...) 
Polygon::Polygon(float circumscribed_circle_radius, unsigned int regular_number_of_points, sf::Color color, float mass)
    :PhysicalObject(),
    fSweptBounds(sf::PrimitiveType::LineStrip), fPoints(sf::PrimitiveType::LineStrip), farthestDistanceFromCentroid(-INFINITY),
     fContactPointVisual(sf::PrimitiveType::Lines), fMainFeatures(sf::PrimitiveType::Lines)
{
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Green));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Green));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Red));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Red));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color(255, 128, 0, 255)));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color(255, 128, 0, 255)));

    fMainFeatures.append(sf::Vertex(sf::Vector2f(), sf::Color::Magenta));
    fMainFeatures.append(sf::Vertex(sf::Vector2f(), sf::Color::Magenta));
    fMainFeatures.append(sf::Vertex(sf::Vector2f(), sf::Color::Cyan));
    fMainFeatures.append(sf::Vertex(sf::Vector2f(), sf::Color::Cyan));

    float exterior_angle = 2.f*PI / regular_number_of_points;
    fPoints.append(sf::Vertex(sf::Vector2f(-circumscribed_circle_radius, 0.f), color));

    for(unsigned int i = 1;i<regular_number_of_points;++i)
    {
        sf::Vector2f last_pos = fPoints[i - 1].position;
        Utility::rotateVector(last_pos, exterior_angle);
        fPoints.append(sf::Vertex(last_pos, color));
    }


    fPoints.append(fPoints[0]);

    computeCentroid();
    fLocalBounds = fPoints.getBounds();

    fSweptBounds = fPoints;
}

Polygon::Polygon(const Polygon& poly)
    :PhysicalObject(poly), fSweptBounds(poly.fSweptBounds), fPoints(poly.fPoints), 
    fContactPointVisual(poly.fContactPointVisual), fMainFeatures(poly.fMainFeatures)
{
    
}

Polygon::~Polygon()
{
}

PhysicalObjectType Polygon::getObjectType() const
{
    return PhysicalObjectType::Polygon;
}

PhysicalObject* Polygon::clonePhysicalObject() const
{
    return new Polygon(*this);
}

std::ostream& operator<<(std::ostream& os, const sf::Vector2f& v)
{
    return os<<v.x<<","<<v.y;
}

/*with the help of this site: https://fotino.me/moment-of-inertia-algorithm/ */
//rotating around p3
float Polygon::computeTriangleInertia(const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3)
{
    sf::Vector2f v1 = p2 - p1;
    float w = Utility::length(v1);
    sf::Vector2f v2 = p3 - p1;
    sf::Vector2f p4 = p1 + Utility::dotProd(v2, v1 / (w * w)) * v1;
    float h = std::abs(Utility::crossProduct2D(v1, v2)) / w;
    float w1 = Utility::length(p4 - p1);
    float w2 = Utility::length(p2 - p4);
    float rho = (fMass / fArea);
    
    /**
     * the formula for a right triangle is rho * (hw³ / 4.f + h³w / 12.f) 
     * but here, our 'h' == w and 'w1' and 'w2' == h
     * 
     */
    float I_1 = rho * h * w1 * (h * h * 3.0 + w1 * w1); I_1 /= 12.0;
    float I_2 = rho * h * w2 * (h * h * 3.0 + w2 * w2); I_2 /= 12.0;

    if(Utility::crossProduct2D(p3 - p2, p4 - p2) < 0.0)
        I_2 *= -1.0;
    if(Utility::crossProduct2D(p3 - p1, p3 - p4) < 0.0)
        I_1 *= -1.0;
    
    return I_1 + I_2;

};

void Polygon::computeInertia()
{    
    if(fMass < 0.f) //check if its mass is infinite
    {
        fInertiaMomentum = std::numeric_limits<float>::max();
        fInverseInertia = 0.f;
        fInverseMass = 0.f;
        return;
    }

    /**
     * we divide the convex shape into triangles and then 
     * compute the sum of all the triangles' inertias 
     * the rotation axis is (origin, Z) where origin is the centroid of the polygon
     * 
     */
    fInertiaMomentum = 0.f;
    for(unsigned int i = 0;i<fPoints.getVertexCount() - 1;++i)
    {
        fInertiaMomentum += computeTriangleInertia(fPoints[i].position, fPoints[i+1].position, getOrigin());
    }

    fInertiaMomentum = std::abs(fInertiaMomentum);
    fInverseInertia = 1.f / fInertiaMomentum;
    fInverseMass = 1.f / fMass;
    
}

float Polygon::computeSignedArea()
{
    float area = 0.f;
    for(unsigned int i = 0;i < fPoints.getVertexCount() - 1;++i)
    {
        area += (fPoints[i].position.x) * (fPoints[i+1].position.y)
            - (fPoints[i+1].position.x) * (fPoints[i].position.y);
    }
    area *= 0.5;

    fArea = std::abs(area);
	return area;
}

void Polygon::computeCentroid()
{
    sf::Vector2f centroid(0.f,0.f);
    float area = computeSignedArea();
    if(std::abs(area) <= __FLT_EPSILON__) return;
    float product_diff = 0.f;

    //computing the centroid
    for(unsigned int i = 0;i<fPoints.getVertexCount() - 1;++i)
    {
        product_diff = fPoints[i].position.x * fPoints[i+1].position.y 
            - fPoints[i+1].position.x * fPoints[i].position.y;
        centroid.x += (fPoints[i].position.x + fPoints[i+1].position.x) * product_diff;
        centroid.y += (fPoints[i].position.y + fPoints[i+1].position.y) * product_diff;
    }
    centroid /= (6.f * area);
    fCentroid = centroid;
    setOrigin(centroid);

    float currentDistance = 0.f;
    //computing the farthest point from the centroid (used by the containsPoint method)
    for(unsigned int i = 0;i<fPoints.getVertexCount() - 1;++i)
    {
        currentDistance = Utility::dotProd(fPoints[i].position - fCentroid, fPoints[i].position -  fCentroid);
        if(currentDistance >= farthestDistanceFromCentroid)
            farthestDistanceFromCentroid = currentDistance;
    }
}


std::vector<sf::Vector2f> Polygon::transformedPoints()
{
    std::vector<sf::Vector2f> points(fPoints.getVertexCount());

    for(unsigned int i = 0;i<fPoints.getVertexCount();++i)
    {
        points[i] = this->getTransform().transformPoint(fPoints[i].position);
    }

    return points;
}
std::vector<sf::Vector2f> Polygon::transformedSweptBounds()
{
    std::vector<sf::Vector2f> points(fSweptBounds.getVertexCount());

    for(unsigned int i = 0;i<fSweptBounds.getVertexCount();++i)
    {
        points[i] = this->getTransform().transformPoint(fSweptBounds[i].position);
    }

    return points;
}


void Polygon::setPolygon(const std::vector<sf::Vector2f>& points)
{
    fPoints.clear();
    
    if(points.size() <= 0)
        return;

    for(auto& p : points)
    {
        fPoints.append(sf::Vertex(p, fColor));
    }

    fPoints.append(sf::Vertex(points[0], fColor));

    computeCentroid();

    fSweptBounds = fPoints;
    fLocalBounds = fPoints.getBounds();
    setRotation(getRotation());
    computeInertia();

}


float Polygon::getSideLength() const
{
    if(fPoints.getVertexCount() > 2)
    {
        return Utility::length(fPoints[1].position - fPoints[0].position);
    }

    return 0.f;
}


std::vector<sf::Vector2f> Polygon::getPointsWithoutTranform() const
{
    std::vector<sf::Vector2f> points(fSweptBounds.getVertexCount());

    for(unsigned int i = 0;i<fSweptBounds.getVertexCount();++i)
    {
        points[i] = fSweptBounds[i].position;
    }

    return points;
}



void Polygon::computeSweptBounds(sf::Vector2f movedOffset)
{
    if(fPoints.getVertexCount() < 2U)
        return;
    //we need to find the points which are of minimum and maximum distance when projected on the
    //axis perpendicular from the movedOffset
    //then we erase the points that are (when projected on the movedOffset) between the two successive positions
    //which is simply between the projections of the min and max point we just calculated, but we also need to keep the right order 
    //in which the vertices are so that the polygon is correctly constructed

    if(Utility::length(movedOffset) <= __FLT_EPSILON__)
        return;
    //first reset the swept bounds
    fSweptBounds.clear();

    float min_proj = INFINITY;  unsigned int minproj_pos = 0;
    float max_proj = -INFINITY; unsigned int maxproj_pos = 0;

    
    
    sf::Vector2f offsetNormal = movedOffset;
    Utility::rotateVector(offsetNormal, PI / 2.f);
    //compute maximum and minimum projected points
    for(unsigned int i = 0;i<fPoints.getVertexCount() - 1;++i)
    {
        float proj = Utility::dotProd(fPoints[i].position, offsetNormal);
        if(proj >= max_proj)
        {
            max_proj = proj;
            maxproj_pos = i;
        }
        if(proj <= min_proj)
        {
            min_proj = proj;
            minproj_pos = i;
        }
    }   
    
    //and then erase the points in between the two successive position while maintaining the right order
    //the polygon is convex so in order to maintain the points' order we need to evaluate which "side" of the polygon is contained inside the swept polygon
    //and which is not, and for that we just have see the projection of the next point
    unsigned int min_index = std::min(minproj_pos, maxproj_pos);
    unsigned int max_index = std::max(minproj_pos, maxproj_pos);
    sf::Vector2f compare_normalVec = fPoints[maxproj_pos].position - fPoints[minproj_pos].position;
    Utility::rotateVector(compare_normalVec, PI / 2.f);
    unsigned int next_index = (max_index + 1) % (fPoints.getVertexCount()-1);

    //need to compare according to the middle point of the normal vector
    float sign_proj = Utility::dotProd(fPoints[next_index].position - fPoints[minproj_pos].position, compare_normalVec);
    
    //here, if the next point is on the side of the movement, then we go to the next point, if not we go to the other way around
    int incrementSign = (sign_proj < 0.f) ? -1 : 1;


    for(unsigned int i = max_index;i%(fPoints.getVertexCount()-1) != min_index; i += incrementSign)
    {
        fSweptBounds.append(sf::Vertex(fPoints[i % (fPoints.getVertexCount()-1)].position, sf::Color::Yellow));
    }
    
    fSweptBounds.append(sf::Vertex(fPoints[min_index].position, sf::Color::Yellow));
    for(unsigned int i = min_index;i%(fPoints.getVertexCount()-1) != max_index;(i == 0 && incrementSign == -1) ? (i = fPoints.getVertexCount() - 2) : i += incrementSign)
    {
        fSweptBounds.append(sf::Vertex(fPoints[i%(fPoints.getVertexCount()-1)].position + movedOffset, sf::Color::Yellow));
    }
    
    fSweptBounds.append(sf::Vertex(fPoints[max_index].position + movedOffset, sf::Color::Yellow));
    fSweptBounds.append(sf::Vertex(fPoints[max_index].position, sf::Color::Yellow));

    
}

void Polygon::setColor(sf::Color new_color)
{
    PhysicalObject::setColor(new_color);
    for(unsigned int i = 0;i<fPoints.getVertexCount();++i)
    {
        fPoints[i].color = fColor;
    }
}

const sf::VertexArray& Polygon::getPoints() const
{
    return fPoints;
}

bool Polygon::doesCollide(PhysicalObject& other, OverlapData& overlap_data)
{
    switch (other.getObjectType())
    {
    case PhysicalObjectType::Polygon:
    {
        Polygon& other_poly = static_cast<Polygon&>(other);
        return doesCollideWithPoly(other_poly, overlap_data);
    }
        break;
    case PhysicalObjectType::Circle:
    {
        PhysicalCircle& circle = static_cast<PhysicalCircle&>(other);
        if(circle.doesCollideWithPoly(*this, overlap_data))
        {
            overlap_data.fCollisionNormal *= -1.f;
            return true;
        }
        return false;
    }
        break;
    default:
        break;
    }

    return false;
}

//structure used by computeContactPoints
struct FeatureEdge
{
    FeatureEdge()
        :edgePoints(), edge(), maxPoint(){}
    FeatureEdge(const sf::Vector2f& v,
                const sf::Vector2f& v1,
                const sf::Vector2f& v2)
            :edgePoints(v1, v2), edge(v2 - v1), maxPoint(v){}

    std::pair<sf::Vector2f, sf::Vector2f> edgePoints;
    sf::Vector2f edge;
    sf::Vector2f maxPoint;
};

//find best feature along collision normal
FeatureEdge Polygon::bestFeature(const sf::Vector2f& normal, 
    const std::vector<sf::Vector2f>& points, bool other = false)
{
    //first compute the vertex that is the furthest away 
    //along the normal
    size_t furthestPointIndex = 0;
    float maxProj = -INFINITY;
    float currentProj = 0.f;
    for(size_t i = 0; i < points.size() - 1;++i)
    {
        currentProj = Utility::dotProd(points[i], normal);
        if(currentProj > maxProj)
        {
            maxProj = currentProj;
            furthestPointIndex = i;
        }
    }

    //then find the edge attached to the furthest
    //point that is most perpendicular to the normal
    sf::Vector2f v = points[furthestPointIndex];
    sf::Vector2f v0 = points[(furthestPointIndex == 0) ? points.size() - 2 : furthestPointIndex - 1];
    sf::Vector2f v1 = points[(furthestPointIndex + 1) % (points.size() - 1)];

    sf::Vector2f right = Utility::normalize(v - v0);
    sf::Vector2f left = Utility::normalize(v - v1);

    //test which edge is 'better' (most perpendicular to the collision normal)
    if(Utility::dotProd(right, normal) <= Utility::dotProd(left, normal))
    {
        //right is better
        if(other)
            return FeatureEdge(v, v, v0);
        return FeatureEdge(v, v0, v);
    }
        
    //left is better
    if(other)
        return FeatureEdge(v, v1, v);
    
    return FeatureEdge(v, v, v1);
    
}

// clips the line segment points v1, v2
// if they are past amount along n
ContactPoints clip(const sf::Vector2f& v1, const sf::Vector2f& v2,
                const sf::Vector2f& n, float amount)
{
    ContactPoints cp;

    float d1 = Utility::dotProd(v1, n) - amount;
    float d2 = Utility::dotProd(v2, n) - amount;

    //if either point is past 'amount' along n, then it is kept
    if(d1 >= 0.f) cp.add(v1);
    if(d2 >= 0.f) cp.add(v2);

    // finally we need to check if they
    // are on opposing sides so that we can
    // compute the correct point
    if(d1 * d2 < 0.f)
    {
        // if they are on different sides of the
        // offset, d1 and d2 will be a (+) * (-)
        // and will yield a (-) and therefore be
        // less than zero
        // get the vector for the edge we are clipping
        sf::Vector2f e = v2 - v1;
        //compute the location along e
        float factor = d1 / (d1 - d2);
        e *= factor;
        e += v1;
        
        //and then add the point
        cp.add(e);
    }

    return cp;
    
}


//compute the different contact points of the collision pair, (this, other).
//A huge thanks to this blog for helping me with that : https://dyn4j.org/2011/11/contact-points-using-clipping/#cpg-clip
void Polygon::computeContactPoints(Polygon& other, OverlapData& overlap_data)
{
    sf::Vector2f n = overlap_data.fCollisionNormal;
    //find the best feature for each polygon
    FeatureEdge e1 = bestFeature(n, transformedSweptBounds());
    
    FeatureEdge e2 = bestFeature(-n, other.transformedSweptBounds(), true);

    FeatureEdge ref, inc;
    bool flip = false;
    if(fabs(Utility::dotProd(Utility::normalize(e1.edge), n)) <= fabs(Utility::dotProd(Utility::normalize(e2.edge), n)))
    {
        ref = e1;
        inc = e2;
    }
    else
    {
        ref = e2;
        inc = e1;
        flip = true;
    }

/*     fMainFeatures[0].position = ref.edgePoints.first;
    fMainFeatures[1].position = ref.edgePoints.second;

    fMainFeatures[2].position = inc.edgePoints.first;
    fMainFeatures[3].position = inc.edgePoints.second; */
    
    //the normalized reference edge vector
    sf::Vector2f refv = Utility::normalize(ref.edge);

    float o1 = Utility::dotProd(ref.edgePoints.first, refv);

    //clip the incident edge by the first vertex of the reference edge
    ContactPoints cp = clip(inc.edgePoints.first, inc.edgePoints.second, refv, o1);
    
    //if we don't have two points, then it went wrong
    if(cp.size < 2U)
        return;

    //then we do the same thing but with what's left of the incident edge after clipping
    //Because it's the other side of the edge, we need to clip in the opposite direction
    float o2 = Utility::dotProd(ref.edgePoints.second, refv);
    cp = clip(cp[0], cp[1], -refv, -o2);
    
    //if we don't have two points, then it went wrong
    if(cp.size < 2U)
        return;


    sf::Vector2f refNormal = Utility::Cross2DScalar(refv, -1.f);

    if(flip)
        refNormal *= -1.f;

    fContactPointVisual[0].position = ref.maxPoint;
    fContactPointVisual[1].position = ref.maxPoint + refNormal * 10.f;

    // get the largest depth
    double max = Utility::dotProd(ref.maxPoint, refNormal);

/*     fContactPointVisual[0].position = getPosition();
    fContactPointVisual[1].position = e1.maxPoint;
    other.fContactPointVisual[0].position = other.getPosition();
    other.fContactPointVisual[1].position = e2.maxPoint; */

    // make sure the final points are not past this maximum
    if(Utility::dotProd(cp[0], refNormal) - max < -contact_epsilon)
    {
        cp.remove(cp[0]);
    }
    else if(Utility::dotProd(cp[1], refNormal) - max < -contact_epsilon)
    {
        cp.remove(cp[1]);
    }

    overlap_data.fContactPoints = cp;
    if(cp.size == 2){
        fContactPointVisual[4].color = sf::Color(255, 128, 0);
        fContactPointVisual[5].color = sf::Color(255, 128, 0);
        fContactPointVisual[4].position = getPosition();
        fContactPointVisual[5].position = cp[1];
    }
    else
    {
        fContactPointVisual[4].color = sf::Color(255, 128, 0, 0);
        fContactPointVisual[5].color = sf::Color(255, 128, 0, 0);
    }
    fContactPointVisual[2].position = getPosition();
    fContactPointVisual[3].position = cp[0];
}

sf::VertexArray Polygon::getTransformedVertices() const
{
    sf::VertexArray vertices(sf::PrimitiveType::LineStrip, fPoints.getVertexCount());
    for(unsigned int i = 0;i<fPoints.getVertexCount();++i)
    {
        vertices[i] = this->getTransform().transformPoint(fPoints[i].position);
    }

    return vertices;
}

bool Polygon::doesCollideWithPoly(Polygon& other, OverlapData& overlap_data)
{
    if(&other == this) return false;
    //Implementation of SAT (Seperated Axis Theorem)
    //check the projection of every edge against every perpendicular axis
    //if there is an axis on which the projections do not intersect, then there is no collision


    std::vector<sf::Vector2f> points_this = transformedSweptBounds();
    auto points_other = other.transformedSweptBounds();


    auto* this_one = &points_this;
    auto* other_one = &points_other;

    float overlap = 0.f;
    overlap_data.fOverlapAmount = INFINITY;

    //we do this for both shapes
    for(int shape = 0;shape<2;++shape)
    {
        for(size_t pointIndex = 0;pointIndex < this_one->size() - 1;++pointIndex)
        {
            //define the axis to project the points on
            sf::Vector2f projAxis = Utility::normalize((*this_one)[pointIndex + 1] -
                (*this_one)[pointIndex]);
            Utility::rotateVector(projAxis, (1 - 2 * shape) * PI * 0.5f);

            //project the points and find minimum projection
            float min_proj_this, min_proj_other, max_proj_this, max_proj_other;
            Utility::projectPointsOnAxis(*this_one, projAxis, min_proj_this, max_proj_this);
            Utility::projectPointsOnAxis(*other_one, projAxis, min_proj_other, max_proj_other);

            //test if there is collision
            overlap = std::min(max_proj_other, max_proj_this) - std::max(min_proj_other, min_proj_this);
            if(overlap < overlap_data.fOverlapAmount)
            {
                overlap_data.fOverlapAmount = overlap;
                overlap_data.fCollisionNormal = projAxis;
            }

            if(overlap < 0.f)
                return false;
        }

        this_one = &points_other;
        other_one = &points_this;
    }
    overlap_data.fCollisionNormal *= 
        Utility::dotProd(getPosition() - other.getPosition(), overlap_data.fCollisionNormal) > 0.f ? -1.f : 1.f;
        
    computeContactPoints(other, overlap_data);
    solveCollisions(other, overlap_data);

    fSweptBounds = fPoints;
    other.fSweptBounds = other.fPoints;
    return true;

}


bool Polygon::containsPoint(const sf::Vector2f& point)
{
    float distanceToPoint = Utility::dotProd(point - getPosition(), point - getPosition());
    if(distanceToPoint > farthestDistanceFromCentroid)  //that eliminates most of the points and so we only do the for loop if the point is within the inscribe circle
        return false;

    //the points are oredered in an anticlockwise manner so if the point is on the right side of any edge, it means it's outside, otherwise it's inside
    for(unsigned int i = 0;i<fPoints.getVertexCount() - 1;++i)
    {
        sf::Vector2f p1 = getTransform().transformPoint(fPoints[i].position);
        sf::Vector2f p2 = getTransform().transformPoint(fPoints[i+1].position);
        if(Utility::crossProduct2D(point - p1, p2 - p1) >= 0.f) //it's on the right side (sin >= 0.f)
            return false;
    }

    return true;
}

void Polygon::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    PhysicalObject::draw(target, states);


    if(drawableEntity)
    {
        states.transform *= getTransform();

        
        target.draw(fPoints, states);
        //target.draw(fSweptBounds, states);
        //target.draw(fMainFeatures);
        //target.draw(fContactPointVisual);
    }



/*     sf::FloatRect box = getBoundingRectangle();
    sf::RectangleShape rect{sf::Vector2f(box.width, box.height)};
    rect.setPosition(box.left, box.top);
    rect.setFillColor(sf::Color::Transparent);
    rect.setOutlineColor(sf::Color::Green);
    rect.setOutlineThickness(2.f);
    target.draw(rect); */

}
