#include "physical_object.hpp"

PhysicalObject::PhysicalObject()
    :fColor(0,0,0), fCentroid(), fLocalBounds(), forces(), torques(0.f), fLinearVelocity()
{}

PhysicalObject::~PhysicalObject()
{
    if(drawableEntity)
        delete drawableEntity;
}

sf::FloatRect PhysicalObject::getBoundingRectangle() const
{
       return getTransform().transformRect(fLocalBounds);
}

void PhysicalObject::update(sf::Time dt)
{}

void PhysicalObject::handleEvent(const sf::Event& event)
{}

void PhysicalObject::setColor(sf::Color new_color)
{
    fColor = new_color;
}

float PhysicalObject::getInverseInertia() const
{
    return fInverseInertia;
}

PhysicalObject::CollisionSolved PhysicalObject::solveCollisions(PhysicalObject& other, OverlapData overlap_data)
{
    sf::Vector2f overlap_amount = overlap_data.fOverlapAmount * overlap_data.fCollisionNormal;
    if(fMass >= 0.f && other.fMass < 0.f)
    {
        move(-overlap_amount);
        return CollisionSolved::ThisMoved;
    }
    else if(other.fMass >= 0.f && fMass < 0.f)
    {
        other.move(overlap_amount);
        return CollisionSolved::OtherMoved;
    }
    else if (other.fMass >= 0.f && fMass >= 0.f)
    {
        move(-overlap_amount * 0.5f);
        other.move(overlap_amount * 0.5f);
        return CollisionSolved::BothMoved;
    }

    return CollisionSolved::NoneMoved;
}

sf::Color PhysicalObject::getColor() const
{
    return fColor;
}

float PhysicalObject::getMass() const
{
    return fMass;
}
float PhysicalObject::getInverseMass() const
{
    return fInverseMass;
}

void PhysicalObject::setMass(float new_mass)
{
    fMass = new_mass;
    computeInertia();
}

void PhysicalObject::applyForce(const sf::Vector2f& force)
{
    forces += force;
}
void PhysicalObject::applyTorque(float torque)
{
    torques += torque;
}

void PhysicalObject::intergrateForcesAndTorque(sf::Time dt)
{
    fLinearVelocity += fInverseMass * forces * dt.asSeconds() * 0.5f;
    fAngularVelocity += fInverseInertia * torques * dt.asSeconds() * 0.5f;
}

void PhysicalObject::clearForces()
{
    forces = sf::Vector2f();
    torques = 0.f;
}

float PhysicalObject::getInertiaMomentum() const
{
    return fInertiaMomentum;
}
float PhysicalObject::getArea() const
{
    return fArea;
}
sf::Vector2f PhysicalObject::getCentroid() const
{
    return fCentroid;
}

void PhysicalObject::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    if(drawableEntity)
    {
        states.transform *= getTransform();
        target.draw(*drawableEntity, states);
    }
    
}

ContactPoints::ContactPoints()
        :size(0), points{sf::Vector2f(), sf::Vector2f()}
{}

ContactPoints::ContactPoints(const ContactPoints &other)
    :size(other.size), points{other.points[0], other.points[1]}
{}

sf::Vector2f &ContactPoints::operator[](size_t index)
{
    if(index >= size)
        assert(false);
    
    return points[index];
}

void ContactPoints::clear()
{
    size = 0;
}

void ContactPoints::add(const sf::Vector2f &p)
{
    if(size >= 2) return;

    points[size++] = p;
}

void ContactPoints::remove(const sf::Vector2f &p)
{
    if(size == 0) return;

    if(points[size - 1] == p)
    {
        size -= 1;
    }
    else if(points[0] == p)
    {
        points[0] = points[1];
        size -= 1;
    }
}