#include "simple_convex_entity.hpp"

SimpleConvexEntity::SimpleConvexEntity()
    :shape()
{

}

SimpleConvexEntity::~SimpleConvexEntity()
{

}


void SimpleConvexEntity::update(sf::Time dt)
{

}

void SimpleConvexEntity::handleEvent(const sf::Event& e)
{

}


void SimpleConvexEntity::initDrawableEntityViaJSON(const nlohmann::json& entity_data)
{
    DrawableEntity::initDrawableEntityViaJSON(entity_data);

    shape.setFillColor(sf::Color(entity_data["FillColor"]));
    shape.setOutlineColor(sf::Color(entity_data["OutlineColor"]));
    shape.setOutlineThickness(entity_data["OutlineThickness"]);

    shape.setPointCount(entity_data["shape_vertices"].size());
    for(unsigned int i = 0;i<shape.getPointCount();++i)
    {
        shape.setPoint(i, sf::Vector2f(entity_data["shape_vertices"][i][0], entity_data["shape_vertices"][i][1]));
    }
}

DrawableEntity* SimpleConvexEntity::cloneEntity() const
{
    return new SimpleConvexEntity(*this);
}

ObjectType::ID SimpleConvexEntity::getObjectType() const
{
    return ObjectType::SimplePolygon;
}

sf::FloatRect SimpleConvexEntity::getBoundingRect() const
{
    return getTransform().transformRect(shape.getGlobalBounds());
}


void SimpleConvexEntity::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(shape, states);
}
