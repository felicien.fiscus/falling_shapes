#include "simple_circle_entity.hpp"
#include "physical_object.hpp"

SimpleCircleEntity::SimpleCircleEntity(float radius)
    : circle(radius)
{
    circle.setFillColor(sf::Color::Red);
}

SimpleCircleEntity::SimpleCircleEntity()
    : circle()
{

}

SimpleCircleEntity::~SimpleCircleEntity()
{

}


void SimpleCircleEntity::update(sf::Time dt)
{

}

void SimpleCircleEntity::handleEvent(const sf::Event& e)
{

}


void SimpleCircleEntity::initDrawableEntityViaJSON(const nlohmann::json& entity_data)
{
    DrawableEntity::initDrawableEntityViaJSON(entity_data);
    
    circle.setFillColor(sf::Color(entity_data["FillColor"]));
    circle.setOutlineColor(sf::Color(entity_data["OutlineColor"]));
    circle.setOutlineThickness(entity_data["OutlineThickness"]);
    circle.setRadius(entity_data["radius"]);

}

DrawableEntity* SimpleCircleEntity::cloneEntity() const
{
    return new SimpleCircleEntity(*this);
}

ObjectType::ID SimpleCircleEntity::getObjectType() const
{
    return ObjectType::SimpleCircle;
}

sf::FloatRect SimpleCircleEntity::getBoundingRect() const
{
    if(!parentBounds)
        return getTransform().transformRect(circle.getGlobalBounds());

    return parentBounds->getTransform().transformRect(circle.getGlobalBounds());
}


void SimpleCircleEntity::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(circle, states);
}
