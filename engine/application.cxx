#include "application.hpp"
#include "../maths_operations.hpp"
#include "polygon.hpp"
#include "physical_circle.hpp"
#include "drawable_entity.hpp"
#include <fstream>
#include <numeric>

Application::Application()
    :fWindow(sf::VideoMode(WIDTH, HEIGHT), "Bouncing Shapes", sf::Style::Close),
    fFrameRate(sf::seconds(1.f/60.f)), fRealFrameRateText(), fTextures(), fFonts(), fEngine(fWindow, 6),
    fAttachedObj(nullptr), mousePositionBefore(), nonPhysicalEntities()
{
    loadFonts();

    fRealFrameRateText.setFont(fFonts.get(Fonts::ID::Stats));
    fRealFrameRateText.setCharacterSize(10U);
    fRealFrameRateText.setPosition(5.f, 5.f);

    importShapes("../poly_maker/custom_shapes.json");

    createRenderWindowPhysicalBounds();
}

void Application::createRenderWindowPhysicalBounds()
{
    float half_w = 25.f;
    Polygon* floor_poly = new Polygon();
    floor_poly->setColor(sf::Color::Black);
    floor_poly->setPolygon({sf::Vector2f(0.f, 0.f), sf::Vector2f(WIDTH, 0.f), sf::Vector2f(WIDTH, 2.f * half_w), sf::Vector2f(0.f, 2.f * half_w)});
    floor_poly->setPosition(WIDTH * 0.5f, HEIGHT + half_w);
    floor_poly->setMass(-1.f);
    floor_poly->fRestitutionCoeff = .5f;
    
    fEngine.addObject(floor_poly);

    Polygon* ceiling_poly = new Polygon();
    ceiling_poly->setColor(sf::Color::Red);
    ceiling_poly->setPolygon({sf::Vector2f(0.f, 0.f), sf::Vector2f(WIDTH, 0.f), sf::Vector2f(WIDTH, 2.f * half_w), sf::Vector2f(0.f, 2.f * half_w)});
    ceiling_poly->setPosition(WIDTH * 0.5f, -half_w);
    ceiling_poly->setMass(-1.f);
    ceiling_poly->fRestitutionCoeff = .5f;
    
    fEngine.addObject(ceiling_poly);

    Polygon* right_poly = new Polygon();
    right_poly->setColor(sf::Color::Black);
    right_poly->setPolygon({sf::Vector2f(0.f, 0.f), sf::Vector2f(2.f * half_w, 0.f), sf::Vector2f(2.f * half_w, HEIGHT), sf::Vector2f(0.f, HEIGHT)});
    right_poly->setPosition(WIDTH + half_w, HEIGHT * 0.5f);
    right_poly->setMass(-1.f);
    right_poly->fRestitutionCoeff = .5f;
    
    fEngine.addObject(right_poly);

    Polygon* left_poly = new Polygon();
    left_poly->setColor(sf::Color::Red);
    left_poly->setPolygon({sf::Vector2f(0.f, 0.f), sf::Vector2f(2.f * half_w, 0.f), sf::Vector2f(2.f * half_w, HEIGHT), sf::Vector2f(0.f, HEIGHT)});
    left_poly->setPosition(-half_w, HEIGHT * 0.5f);
    left_poly->setMass(-1.f);
    left_poly->fRestitutionCoeff = .5f;
    
    fEngine.addObject(left_poly);
}

Application::~Application()
{
}

PhysicalCircle* Application::initCircleImport(const nlohmann::json& shape_data)
{
    PhysicalCircle* circle = new PhysicalCircle(shape_data["radius"]);
    circle->setPosition(shape_data["boundsPosition"][0], shape_data["boundsPosition"][1]);
    return circle;
}
Polygon* Application::initPolygonImport(const nlohmann::json& shape_data)
{
    Polygon* poly = new Polygon();
    std::vector<sf::Vector2f> points;
    for(auto& coords_pair : shape_data["poly_vertices"])
    {
        points.push_back(sf::Vector2f(coords_pair[0], coords_pair[1]));
    }

    poly->setPolygon(points);
    poly->setPosition(poly->getCentroid());

    return poly;
}


void Application::initShapePhysicalParams(PhysicalObject* shape, const nlohmann::json& shape_data)
{
    if(!shape_data["isStatic"])
    {
        shape->setMass(shape_data["mass"]);
        shape->fGravitySensitive = true;
    }
    else
    {
        shape->setMass(-1.f);
        shape->fGravitySensitive = false;
    }
    shape->fInitialRestCoeff = .5f;
    shape->fRestitutionCoeff = shape->fInitialRestCoeff;
}

void Application::importSimplePhysicalObject(const nlohmann::json& shape_data, DrawableEntity* associated_entity)
{
    PhysicalObject* physicalShape = nullptr;
    if(shape_data["boundsType"] == "circle")
    {
        physicalShape = initCircleImport(shape_data);
    }
    else if(shape_data["boundsType"] == "polygon")
    {
        physicalShape = initPolygonImport(shape_data);
    }
    else
        throw std::runtime_error("unknown shape type when parsing through json in importShapes");

    initShapePhysicalParams(physicalShape, shape_data);
    physicalShape->drawableEntity = associated_entity;
    associated_entity->parentBounds = physicalShape;

    fEngine.addObject(physicalShape);

}

void Application::importShapes(const std::string& input_file)
{
    std::ifstream input{input_file};
    if(!input.is_open())
        throw std::runtime_error("unable to open file: " + input_file);

    nlohmann::json jsonShapes;

    input >> jsonShapes;

    input.close();
    
    for(auto& shape : jsonShapes)
    {
        DrawableEntity* entity = DrawableEntity::getInstanceOfDrawableEntityFromType(shape["type"], fTextures);
        if((int)shape["type"] & (int)ObjectType::SimplePhysicalObject)
        {
            importSimplePhysicalObject(shape, entity);
        }
        else
        {
            nonPhysicalEntities.push_front(entity);   
        }
        entity->initDrawableEntityViaJSON(shape);
    
    }

}

void Application::run()
{
    sf::Clock appClock;
    sf::Time accu = sf::Time::Zero, deltaTime = sf::Time::Zero;
    sf::Time realFrameRateAccu = sf::Time::Zero;

    while(fWindow.isOpen())
    {
        deltaTime = appClock.restart();
        accu += deltaTime;
        realFrameRateAccu += deltaTime;
        //update the application
        while(accu >= fFrameRate)
        {
            accu -= fFrameRate;

            handleEvent();
            update();


            if(take_screenshots)
            {
                sf::Texture texture;
                texture.create(fWindow.getSize().x, fWindow.getSize().y);
                texture.update(fWindow);
                fWindowScreenshots.push_back(texture);
            }
        } 

        //update the real frame rate displaying text
        if(realFrameRateAccu >= sf::seconds(0.5f))
        {
            realFrameRateAccu = sf::Time::Zero;

            fRealFrameRateText.setString(std::to_string((int)(1 / deltaTime.asSeconds())) + " fps\n" + std::to_string(deltaTime.asMicroseconds()) + "us");
        }
        
        render();
    }
}

void Application::keyEvents(const sf::Event& event)
{
    sf::Vector2f mouse_pos(sf::Mouse::getPosition(fWindow));
    switch (event.key.code)
    {
    case sf::Keyboard::Escape:
        fWindow.close();
        break;
    case sf::Keyboard::R:
        take_screenshots = !take_screenshots;
        if(take_screenshots)
            fWindowScreenshots.clear();
    break;
    case sf::Keyboard::I:
        for(auto& texture : fWindowScreenshots)
        {
            if (!texture.copyToImage().saveToFile("images/image_" + std::to_string(image_incr++) + ".png"))
            {
                std::cerr << "screenshot failed to saved" << std::endl;
            }
        }
    break;
    case sf::Keyboard::Space:
        cloneAttachedShape(mouse_pos);
    break;
    default:
        break;
    }

}

void Application::cloneAttachedShape(const sf::Vector2f& mouse_pos)
{
    if(mouse_pos.x < WIDTH && mouse_pos.y <= HEIGHT 
        && mouse_pos.x > 0.f && mouse_pos.y > 0.f)
    {
        if(fAttachedObj)
        {

            PhysicalObject* new_obj = fAttachedObj->clonePhysicalObject();
            if(fAttachedObj->drawableEntity)
            {
                new_obj->drawableEntity = fAttachedObj->drawableEntity->cloneEntity();
                new_obj->drawableEntity->parentBounds = new_obj;
            }
            new_obj->fGravitySensitive = true;

            fEngine.addObject(new_obj);
        
        }
    }
}

void Application::attachObjectToCursor(PhysicalObject* obj)
{
    fAttachedObj = obj;
    fEngine.removeGravityOnObject(obj);
    obj->fLinearVelocity = sf::Vector2f();
    obj->setPosition(sf::Vector2f(sf::Mouse::getPosition(fWindow)));
}

void Application::handleEvent()
{
    sf::Event event;
    while(fWindow.pollEvent(event))
    {
        switch (event.type)
        {
        case sf::Event::Closed:
            fWindow.close();
            break;
        case sf::Event::KeyPressed:
            keyEvents(event);
            break;
        break;
        case sf::Event::MouseButtonPressed:
            isObjAttached = !isObjAttached;
            if(isObjAttached)
            {
                sf::Vector2f mouse_pos(sf::Mouse::getPosition(fWindow));
                if(mouse_pos.x < WIDTH && mouse_pos.y <= HEIGHT 
                && mouse_pos.x > 0.f && mouse_pos.y > 0.f)
                {
                    for(auto* obj : fEngine)
                    {
                        if(obj->fGravitySensitive && obj->getBoundingRectangle().contains(mouse_pos))
                        {
                            attachObjectToCursor(obj);
                            break;
                        }
                    }
                }
            }
            else if(fAttachedObj)
            {
                fEngine.addGravityOnObject(fAttachedObj);
                fAttachedObj = nullptr;
            }
        break;
        default:
            break;
        }

        fEngine.handleEvent(event);
        for(auto* entity : nonPhysicalEntities)
            entity->handleEvent(event);
    }
}


void Application::add_poly(float posx, float posy)
{
    Polygon* poly = new Polygon(40.f,  4, sf::Color::White, 0.5f);
    poly->setPosition(posx, posy);
    poly->computeCentroid();
    poly->setColor(sf::Color::Blue);
    poly->setMass(2.f);
    poly->fRestitutionCoeff = .5f;
    poly->fInitialRestCoeff = poly->fRestitutionCoeff;
    poly->fGravitySensitive = true;
    poly->rotate(45.f);

    fEngine.addObject(poly);
}

void Application::add_circle(float posx, float posy)
{
    PhysicalCircle* circle = new PhysicalCircle(40.f);
    circle->setPosition(posx, posy);
    circle->setColor(sf::Color::Blue);
    circle->setMass(.5f);
    circle->fRestitutionCoeff = .5f;
    circle->fInitialRestCoeff = circle->fRestitutionCoeff;
    circle->fGravitySensitive = true;

    fEngine.addObject(circle);   
}

void Application::update()
{
    if(fAttachedObj)
    {
        sf::Vector2f current_mousePos(sf::Mouse::getPosition(fWindow));
        fAttachedObj->fLinearVelocity = (current_mousePos - mousePositionBefore) / fFrameRate.asSeconds();
        fAttachedObj->setPosition(current_mousePos);
    }
    fEngine.update(fFrameRate); 
    for(auto* entity: nonPhysicalEntities)
        entity->update(fFrameRate);


    mousePositionBefore = sf::Vector2f(sf::Mouse::getPosition(fWindow));
}

void Application::render()
{
    fWindow.clear();

    for(auto* entity : nonPhysicalEntities)
        fWindow.draw(*entity);

    fWindow.draw(fEngine);

    fWindow.draw(fRealFrameRateText);
    fWindow.display();
}

void Application::loadFonts()
{
    fFonts.load("../resources/fonts/calibri.ttf", Fonts::ID::Stats);
}