#ifndef PHYSICS_ENGINE_HPP
#define PHYSICS_ENGINE_HPP
#include "physical_object.hpp"
#include <list>

struct CollisionPair
{
    CollisionPair(PhysicalObject* _a, PhysicalObject* _b, OverlapData d)
        :a(_a), b(_b), data(d){}
    PhysicalObject* a;
    PhysicalObject* b;
    OverlapData data;
};

class PhysicsEngine : public sf::Drawable
{
public:
    PhysicsEngine(const sf::RenderWindow& window, unsigned int sub_steps = 8);
    ~PhysicsEngine();

    void addObject(PhysicalObject* obj);
    void removeObject(PhysicalObject* obj);

    void removeGravityOnObject(PhysicalObject* obj);
    void addGravityOnObject(PhysicalObject* obj);

    float fGravityConstant = 9.81f;

    void update(sf::Time dt);
    void handleEvent(const sf::Event& event);

    std::vector<PhysicalObject*>::iterator begin();
    std::vector<PhysicalObject*>::iterator end();

    std::list<PhysicalObject*>& getNonStaticObjects();

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    std::vector<PhysicalObject*> fObjects;
    std::list<PhysicalObject*> fGravitySensitiveObjects;
    std::vector<PhysicalObjectData> fInitialValues;
    const sf::RenderWindow& fWindow;
    std::list<CollisionPair> fCollisionPairs;
    unsigned int fSubsteps = 8;

    void integrateForces(sf::Time dt);
    void applyGravity();
    void computeSweptBounds(sf::Time dt);
    void clearForces();
    void updatePositions(sf::Time dt);
    void updateVelocities();
    void computeCollisions();
    void solveCollisions();


    //true -> start, false -> stop
    bool fStartStop = false;
    sf::Time fTimeSinceStart = sf::Time::Zero;
};

#endif //PHYSICS_ENGINE_HPP