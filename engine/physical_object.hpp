#ifndef PHYSICAL_OBJECT_HPP
#define PHYSICAL_OBJECT_HPP
#include "../resource_identifiers.hpp"
#include "../maths_operations.hpp"
#include "drawable_entity.hpp"
#include <array>
#define PI 3.14159265359


struct PhysicalObjectData
{
    sf::Vector2f fInitialPosition;
    float fInitialRotation; 
    float fInitialRestitutionCoeff;
    sf::Vector2f fInitialLinearVelocity;
    float fInitialAngularVelocity;
    float fInitialMass;
};

struct ContactPoints
{
    size_t size;
    sf::Vector2f points[2];

    ContactPoints();

    ContactPoints(const ContactPoints& other);

    sf::Vector2f& operator[](size_t index);

    void clear();

    void add(const sf::Vector2f& p);

    void remove(const sf::Vector2f& p);
};

struct OverlapData
{
    OverlapData()
        :fCollisionNormal(), fOverlapAmount(0.f), fContactPoints()
    {}
    sf::Vector2f fCollisionNormal;
    float fOverlapAmount;
    ContactPoints fContactPoints;
};

enum class PhysicalObjectType
{
    Polygon, Circle
};

class PhysicalObject : public sf::Drawable, public sf::Transformable
{
protected:
    sf::Color fColor;
    float fInertiaMomentum = 0.f;
    float fInverseInertia = std::numeric_limits<float>::max();
    float fArea = 0.f;
    sf::Vector2f fCentroid;
    sf::FloatRect fLocalBounds;

    sf::Vector2f forces;
    float torques;

    enum class CollisionSolved
    {
        ThisMoved, OtherMoved, BothMoved, NoneMoved
    };

    float fMass = 0.5f; //in kg
    float fInverseMass = 2.f;

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
public:
    PhysicalObject();
    virtual ~PhysicalObject();

    //returns the smallest possible rectangle inside which the physical object can be 
    virtual sf::FloatRect getBoundingRectangle() const;

    virtual void update(sf::Time dt);
    virtual void handleEvent(const sf::Event& event);

    //color getters and setters, that is the color of the mesh
    virtual void setColor(sf::Color new_color);
    virtual sf::Color getColor() const;

    //the resulting overlap data (if they collide) is stored in overlap_data
    virtual bool doesCollide(PhysicalObject& other, OverlapData& overlap_data) = 0;
    
    //to call only when initial inertia has been modified, for example a change in mass.
    virtual void computeInertia() = 0;

    //needs to be called before moving the object
    virtual void computeSweptBounds(sf::Vector2f movedOffset) = 0;
    
    virtual float getInertiaMomentum() const;
    virtual float getInverseInertia() const;

    //only clones the the object itself, not the drawable entity potentially attached to it
    virtual PhysicalObject* clonePhysicalObject() const = 0;

	virtual float getArea() const;
    float getMass() const;
    float getInverseMass() const;

    void setMass(float new_mass);

    //adds a new vector of forces / torque to the objects total sum of forces / torques 
    void applyForce(const sf::Vector2f& force);
    void applyTorque(float torque);
    //resets forces and torques to be zero
    void clearForces();

    //modifies the linear and angular velocities to take the forces / torques in account
    //using newton's law : acceleration = inverse_mass * sum of all forces
    //and velocity = acceleration * time
    void intergrateForcesAndTorque(sf::Time dt);

    virtual sf::Vector2f getCentroid() const;

    virtual PhysicalObjectType getObjectType() const = 0;

    //used to solve collision between this object and another physicalObject colliding with this object
    CollisionSolved solveCollisions(PhysicalObject& other, OverlapData overlap_data);

    //see Polygon.cxx or Circle.cxx for implementations, it's in O(polygonSides) for polygon
    virtual bool containsPoint(const sf::Vector2f& point) = 0;

    sf::Vector2f fLinearVelocity;
    float fAngularVelocity = 0.f;
    float fRestitutionCoeff = 0.f;
    float fInitialRestCoeff = 1.f;
    bool fGravitySensitive = false;
    float fStaticFriction = 1.f;
    float fDynamicFriction = 0.48f;

    DrawableEntity* drawableEntity = nullptr;
};

#endif //PHYSICAL_OBJECT_HPP