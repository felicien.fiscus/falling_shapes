#include "simple_sprite_entity.hpp"
#include "physical_object.hpp"
#include "physical_circle.hpp"

SimpleSpriteEntity::SimpleSpriteEntity(TextureHolder& textures_)
    :textures(textures_), sprite()
{

}

SimpleSpriteEntity::~SimpleSpriteEntity()
{

}


void SimpleSpriteEntity::update(sf::Time dt)
{

}

void SimpleSpriteEntity::handleEvent(const sf::Event& e)
{

}


void SimpleSpriteEntity::initDrawableEntityViaJSON(const nlohmann::json& entity_data)
{
    DrawableEntity::initDrawableEntityViaJSON(entity_data);
    

    sf::Texture* texture = nullptr;
    if(!(texture = textures.isResourceFileAlreadyLoaded(entity_data["textureFileName"])))
    {
        unsigned int index = textures.getNumberOfResourceLoaded() + Textures::TexturesCount;
        textures.load(TEXTURE_DIR + (std::string)entity_data["textureFileName"], (Textures::ID)index);
        texture = &textures.get((Textures::ID)index);
    }

    sprite.setTexture(*texture);
    sprite.setTextureRect(sf::IntRect(entity_data["textureRectangle"][0], entity_data["textureRectangle"][1], entity_data["textureRectangle"][2], entity_data["textureRectangle"][3]));
}

DrawableEntity* SimpleSpriteEntity::cloneEntity() const
{
    return new SimpleSpriteEntity(*this);
}

ObjectType::ID SimpleSpriteEntity::getObjectType() const
{
    return ObjectType::SimpleSprite;
}

sf::FloatRect SimpleSpriteEntity::getBoundingRect() const
{
    return getTransform().transformRect(sprite.getGlobalBounds());
}


void SimpleSpriteEntity::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(sprite, states);
}
