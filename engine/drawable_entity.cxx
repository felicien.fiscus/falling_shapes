#include "drawable_entity.hpp"
#include "all_drawable_entities_include.hpp"


DrawableEntity* DrawableEntity::getInstanceOfDrawableEntityFromType(ObjectType::ID type, TextureHolder& textures)
{
    switch (type)
    {
    case ObjectType::SimpleCircle:
        return new SimpleCircleEntity();
        break;
    case ObjectType::SimplePolygon:
        return new SimpleConvexEntity();
        break;
    case ObjectType::SimpleSprite:
        return new SimpleSpriteEntity(textures);
        break;
    
    default:
        break;
    }

    throw std::runtime_error("Unknown object type for getInstanceOfDrawableEntityFromType");
}

DrawableEntity::DrawableEntity()
{
}

DrawableEntity::~DrawableEntity()
{
}

void DrawableEntity::update(sf::Time dt)
{
}

void DrawableEntity::handleEvent(const sf::Event &e)
{
}

void DrawableEntity::initDrawableEntityViaJSON(const nlohmann::json &entity_data)
{
    setOrigin(entity_data["origin"][0], entity_data["origin"][1]);
    setPosition(entity_data["position"][0], entity_data["position"][1]);
    setScale(entity_data["scale"][0], entity_data["scale"][1]);
    setRotation(entity_data["rotation"]);
}

DrawableEntity* DrawableEntity::cloneEntity() const
{
    return new DrawableEntity(*this);
}

ObjectType::ID DrawableEntity::getObjectType() const
{
    return ObjectType::ID::None;
}

sf::FloatRect DrawableEntity::getBoundingRect() const
{
    return sf::FloatRect();
}

void DrawableEntity::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    return;
}
