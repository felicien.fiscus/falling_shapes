#ifndef DRAWABLE_ENTITY_HPP
#define DRAWABLE_ENTITY_HPP
#include "../resource_identifiers.hpp"
#include "../json.hpp"

class DrawableEntity : public sf::Drawable, public sf::Transformable
{
public:
    DrawableEntity();
    virtual ~DrawableEntity();

    static DrawableEntity* getInstanceOfDrawableEntityFromType(ObjectType::ID type, TextureHolder& textures);

    virtual void update(sf::Time dt);
    virtual void handleEvent(const sf::Event& e);

    virtual void initDrawableEntityViaJSON(const nlohmann::json& entity_data);
    //clones the instance but doesn't clone the parentBounds, so they will have the same parent bounds
    virtual DrawableEntity* cloneEntity() const;

    virtual ObjectType::ID getObjectType() const;
    virtual sf::FloatRect getBoundingRect() const;

    class PhysicalObject* parentBounds = nullptr;
    class PhysicsEngine* contextEngine = nullptr;
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};


#endif //DRAWABLE_ENTITY_HPP