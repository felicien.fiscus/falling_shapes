#ifndef SIMPLE_SPRITE_ENTITY_HPP
#define SIMPLE_SPRITE_ENTITY_HPP
#include "drawable_entity.hpp"

class SimpleSpriteEntity : public DrawableEntity
{
public:
    SimpleSpriteEntity(TextureHolder& textures_);
    virtual ~SimpleSpriteEntity() override;

    virtual void update(sf::Time dt) override;
    virtual void handleEvent(const sf::Event& e) override;

    virtual void initDrawableEntityViaJSON(const nlohmann::json& entity_data) override;
    virtual DrawableEntity* cloneEntity() const override;

    virtual ObjectType::ID getObjectType() const override;
    virtual sf::FloatRect getBoundingRect() const override;

protected:
    TextureHolder& textures;
    sf::Sprite sprite;

private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

};

#endif //SIMPLE_SPRITE_ENTITY_HPP