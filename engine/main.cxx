#include <iostream>
#include "application.hpp"


int main(int argc, char** argv)
{
    try
    {
        Application appli;

        appli.run();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    return 0;
}