#ifndef APPLICATION_HPP
#define APPLICATION_HPP
#include "../resource_identifiers.hpp"
#include "physics_engine.hpp"
#include "../json.hpp"

class Application
{

public:
    Application();
    ~Application();

    void run();


private:

    int image_incr = 0;
    std::vector<sf::Texture> fWindowScreenshots;
    bool take_screenshots = false;

    sf::RenderWindow fWindow;
    sf::Time fFrameRate;
    
    sf::Text fRealFrameRateText;
    
    TextureHolder fTextures;
    unsigned int textureNumber = Textures::ID::TexturesCount;
    FontHolder fFonts;
    void loadFonts();

    void attachObjectToCursor(PhysicalObject* obj);

    PhysicsEngine fEngine;
    PhysicalObject* fAttachedObj;
    bool isObjAttached = false;
    sf::Vector2f mousePositionBefore;

    std::list<class DrawableEntity*> nonPhysicalEntities;
    std::list<class Handler*> handlers;

    void add_poly(float posx, float posy);
    void add_circle(float posx, float posy);
    void cloneAttachedShape(const sf::Vector2f& mouse_pos);

    void initShapePhysicalParams(PhysicalObject* shape, const nlohmann::json& shape_data);
    class PhysicalCircle* initCircleImport(const nlohmann::json& shape_data);
    class Polygon* initPolygonImport(const nlohmann::json& shape_data);

    void createRenderWindowPhysicalBounds();

    void update();
    void handleEvent();
    void render();

    void keyEvents(const sf::Event& event);

    void importShapes(const std::string& input_file);
    void importSimplePhysicalObject(const nlohmann::json& shape_data, DrawableEntity* associated_entity);

};

#endif //APPLICATION_HPP