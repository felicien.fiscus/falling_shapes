#ifndef SIMPLE_CIRCLE_ENTITY_HPP
#define SIMPLE_CIRCLE_ENTITY_HPP
#include "drawable_entity.hpp"

class SimpleCircleEntity : public DrawableEntity 
{
public:
    SimpleCircleEntity(float radius);
    SimpleCircleEntity();
    ~SimpleCircleEntity();

    void update(sf::Time dt);
    void handleEvent(const sf::Event& e);

    void initDrawableEntityViaJSON(const nlohmann::json& entity_data);
    DrawableEntity* cloneEntity() const override;

    ObjectType::ID getObjectType() const;
    sf::FloatRect getBoundingRect() const;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    sf::CircleShape circle;
};


#endif //SIMPLE_CIRCLE_ENTITY_HPP