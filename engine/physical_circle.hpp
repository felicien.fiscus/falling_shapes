#ifndef PHYSICAL_CIRCLE_HPP
#define PHYSICAL_CIRCLE_HPP
#include "physical_object.hpp"

class PhysicalCircle : public PhysicalObject
{
public:
    PhysicalCircle(float radius = 0.f);
    
    void setColor(sf::Color new_color) override;    //same as setOutlineColor
    void setRadius(float new_radius);
    float getRadius() const;

    void setOutlineThickness(float thickness);
    void setFillColor(sf::Color color);
    sf::Color getFillColor() const;
    float getOutlineThickness() const;
    
    bool doesCollide(PhysicalObject& other, OverlapData& overlap_data) override;
    bool doesCollideWithPoly(class Polygon& other, OverlapData& overlap_data);
    bool doesCollideWithCircle(PhysicalCircle& other, OverlapData& overlap_data);

    PhysicalObject* clonePhysicalObject() const override;

    void computeInertia() override;
    void computeSweptBounds(sf::Vector2f movedOffset) override;

    sf::Vector2f closestPointToCircle(const std::vector<sf::Vector2f>& points);

    PhysicalObjectType getObjectType() const override;

    sf::FloatRect getBoundingRectangle() const override;

    bool containsPoint(const sf::Vector2f& point);

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    sf::CircleShape fCircle;

    void projectCircle(float& min_proj, float& max_proj, const sf::Vector2f& proj_axis);
    //for debug purposes only
    sf::VertexArray fContactPointVisual;
};

#endif //PHYSICAL_CIRCLE_HPP