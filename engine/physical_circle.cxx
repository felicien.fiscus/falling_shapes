#include "physical_circle.hpp"
#include "polygon.hpp"
#include <algorithm>

PhysicalCircle::PhysicalCircle(float radius)
    :PhysicalObject(), fCircle(radius), fContactPointVisual(sf::Lines)
{
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Green));
    fContactPointVisual.append(sf::Vertex(sf::Vector2f(), sf::Color::Green));
    fCircle.setFillColor(sf::Color::Transparent);
    fCircle.setOutlineThickness(1.f);
    fArea = PI * radius * radius;
    fCentroid.x = radius;
    fCentroid.y = radius;
    setOrigin(fCentroid);
    fLocalBounds.width = 2.f * radius;
    fLocalBounds.height = 2.f * radius;
    computeInertia();
}

void PhysicalCircle::setColor(sf::Color new_color)
{
    PhysicalObject::setColor(new_color);
    fCircle.setOutlineColor(fColor);
}

sf::Color PhysicalCircle::getFillColor() const
{
    return fCircle.getFillColor();
}
float PhysicalCircle::getOutlineThickness() const
{
    return fCircle.getOutlineThickness();
}

sf::FloatRect PhysicalCircle::getBoundingRectangle() const
{
    sf::Vector2f position(getPosition() - fCentroid + sf::Vector2f(fLocalBounds.left, fLocalBounds.top));
    sf::Vector2f dimensions(fLocalBounds.width, fLocalBounds.height);

    return sf::FloatRect(position, dimensions);
}

bool PhysicalCircle::containsPoint(const sf::Vector2f& point)
{
    float radius = fCircle.getRadius();
    return Utility::dotProd(point - getPosition(), point - getPosition()) <= radius * radius;
}

void PhysicalCircle::setRadius(float new_radius)
{
    fCircle.setRadius(new_radius);
    fArea = PI * new_radius * new_radius;
    computeInertia();
    fLocalBounds.width = 2.f * new_radius;
    fLocalBounds.height = 2.f * new_radius;
}

float PhysicalCircle::getRadius() const
{
    return fCircle.getRadius();
}

bool PhysicalCircle::doesCollide(PhysicalObject& other, OverlapData& overlap_data)
{
    switch (other.getObjectType())
    {
    case PhysicalObjectType::Circle:
    {
        PhysicalCircle& circle = static_cast<PhysicalCircle&>(other);
        return doesCollideWithCircle(circle, overlap_data);
    }
        break;
    case PhysicalObjectType::Polygon:
    {
        Polygon& poly = static_cast<Polygon&>(other);
        return doesCollideWithPoly(poly, overlap_data);
    }
        break;
    default:
        break;
    }

    return false;
}

void PhysicalCircle::projectCircle(float& min_proj, float& max_proj, const sf::Vector2f& proj_axis)
{
    float current_proj = Utility::dotProd(getPosition(), proj_axis);

    min_proj = std::min(current_proj + getRadius(), current_proj - getRadius());
    max_proj = std::max(current_proj + getRadius(), current_proj - getRadius());
}


bool PhysicalCircle::doesCollideWithPoly(Polygon& other, OverlapData& overlap_data)
{
    //Implementation of SAT (Seperated Axis Theorem)
    //check the projection of every edge against every perpendicular axis
    //if there is an axis on which the projections do not intersect, then there is no collision


    auto points_other = other.transformedSweptBounds();
    auto* other_one = &points_other;
    //other_one->pop_back();

    overlap_data.fOverlapAmount = INFINITY;
    float overlap = 0.f;    //overlap computed at each iteration

    float min_proj_this = 0.f, max_proj_this = 0.f;
    float min_proj_other = 0.f, max_proj_other = 0.f;
    
    for(int k = 0;k < (int)(other_one->size() - 1);++k)
    {
        //first compute the axis to project on
        sf::Vector2f axis((*other_one)[k+1] - (*other_one)[k]);
        Utility::rotateVector(axis, PI/2.f);
        axis /= Utility::length(axis);

        //then project every point on it and find min / max
        Utility::projectPointsOnAxis(*other_one, axis, min_proj_other, max_proj_other);

        //points of other_one
        projectCircle(min_proj_this, max_proj_this, axis);

        //minimum of maximums - maximum of minimums
        overlap = std::min(max_proj_this, max_proj_other) - std::max(min_proj_this, min_proj_other);
        if(overlap < overlap_data.fOverlapAmount)
        {
            overlap_data.fOverlapAmount = overlap;
            overlap_data.fCollisionNormal = axis;
        }
        
        //then test if they do not intersects, if so then other and this do not collide, if not we continue testing
        if(overlap < 0.f)
        {
            return false;
        }
    }
    //if we arrive to this point, it can happen that the polygon is intersecting the bounding box of the circle
    //but not the circle itself, so we check for one last axis that can potentially be a separating axis

    sf::Vector2f last_axis = Utility::normalize(closestPointToCircle(*other_one) - getPosition());

    Utility::projectPointsOnAxis(*other_one, last_axis, min_proj_other, max_proj_other);
    projectCircle(min_proj_this, max_proj_this, last_axis);

    //minimum of maximums - maximum of minimums
    overlap = std::min(max_proj_this, max_proj_other) - std::max(min_proj_this, min_proj_other);
    //then test if they do not intersects, if so then other and this do not collide
    if(overlap < 0.f)
    {
        return false;
    }
    //if we arrive here than they collide but we need to potentially update the normal collision and collision depth
    if(overlap < overlap_data.fOverlapAmount)
    {
        overlap_data.fOverlapAmount = overlap;
        overlap_data.fCollisionNormal = last_axis;
    }

    overlap_data.fCollisionNormal *= 
        Utility::dotProd(getPosition() - other.getPosition(), overlap_data.fCollisionNormal) > 0.f ? -1.f : 1.f;
    
    solveCollisions((PhysicalObject&)other, overlap_data);

    //determining the contact point
    overlap_data.fContactPoints.clear();
    overlap_data.fContactPoints.add(getPosition() + getRadius() * overlap_data.fCollisionNormal);

    fContactPointVisual[0].position = getPosition();
    fContactPointVisual[1].position = overlap_data.fContactPoints[0];

    
    return true;

}

bool PhysicalCircle::doesCollideWithCircle(PhysicalCircle& other, OverlapData& overlap_data)
{
    float r1 = getRadius();
    float r2 = other.getRadius();
    sf::Vector2f c1 = getPosition();
    sf::Vector2f c2 = other.getPosition();
    float collision_test = Utility::length(c2 - c1);

    if(collision_test <= r1 + r2)   //it's colliding
    {
        overlap_data.fCollisionNormal = c2 - c1;
        float normalLength = Utility::length(overlap_data.fCollisionNormal);
        if(normalLength <= __FLT_EPSILON__)
        {
            overlap_data.fCollisionNormal = sf::Vector2f(1.f, 0.f);
            normalLength = 1.f;
        } 
        overlap_data.fCollisionNormal /= normalLength;
        overlap_data.fOverlapAmount = (r1 + r2 - collision_test) * 0.5f;
        overlap_data.fCollisionNormal *= (Utility::dotProd(other.getPosition() - getPosition(), overlap_data.fCollisionNormal) <= 0.f) ? -1.f : 1.f; 
        solveCollisions(other, overlap_data);

        overlap_data.fContactPoints.clear();
        overlap_data.fContactPoints.add(getPosition() + r1 * overlap_data.fCollisionNormal);

        fContactPointVisual[0].position = getPosition();
        fContactPointVisual[1].position = overlap_data.fContactPoints[0];
        return true;
    }

    return false;
}

PhysicalObject* PhysicalCircle::clonePhysicalObject() const
{
    return new PhysicalCircle(*this);
}

void PhysicalCircle::computeInertia()
{
    if(fMass < 0.f) //check if its mas is infinite
    {
        fInertiaMomentum = std::numeric_limits<float>::max();
        fInverseInertia = 0.f;
        fInverseMass = 0.f;
        return;
    }

    float rho = fMass / fArea;
    fInertiaMomentum = rho * PI * std::pow(getRadius(), 4) * 0.5f;
    fInverseInertia = 1.f / fInertiaMomentum;
    fInverseMass = 1.f / fMass;
}


sf::Vector2f PhysicalCircle::closestPointToCircle(const std::vector<sf::Vector2f>& points)
{
    sf::Vector2f min_point;
    float distance_sq = INFINITY;//distance squared to speed up the computation
    float current_distance = 0.f;
    for(auto& p : points)
    {
        current_distance = Utility::dotProd(p - getPosition(), p - getPosition());
        if(distance_sq > current_distance)
        {
            distance_sq = current_distance;
            min_point = p;
        }
    }

    return min_point;
}

void PhysicalCircle::computeSweptBounds(sf::Vector2f movedOffset)
{}

PhysicalObjectType PhysicalCircle::getObjectType() const
{
    return PhysicalObjectType::Circle;
}

void PhysicalCircle::setOutlineThickness(float thickness)
{
    fCircle.setOutlineThickness(thickness);
}
void PhysicalCircle::setFillColor(sf::Color color)
{
    fCircle.setFillColor(color);
}

void PhysicalCircle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    PhysicalObject::draw(target, states);

    if(drawableEntity)
    {
        states.transform *= getTransform();
        
        target.draw(fCircle, states);
    }

/*         sf::FloatRect box = getBoundingRectangle();
        sf::RectangleShape rect{sf::Vector2f(box.width, box.height)};
        rect.setPosition(box.left, box.top);
        rect.setFillColor(sf::Color::Transparent);
        rect.setOutlineColor(sf::Color::Green);
        rect.setOutlineThickness(2.f);
        target.draw(rect); */
        //target.draw(fContactPointVisual);
}