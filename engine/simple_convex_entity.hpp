#ifndef SIMPLE_CONVEX_ENTITY_HPP
#define SIMPLE_CONVEX_ENTITY_HPP
#include "drawable_entity.hpp"

class SimpleConvexEntity : public DrawableEntity 
{
public:
    SimpleConvexEntity();
    ~SimpleConvexEntity();

    void update(sf::Time dt);
    void handleEvent(const sf::Event& e);

    void initDrawableEntityViaJSON(const nlohmann::json& entity_data);
    DrawableEntity* cloneEntity() const override;

    ObjectType::ID getObjectType() const;
    sf::FloatRect getBoundingRect() const;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    sf::ConvexShape shape;
};

#endif //SIMPLE_CONVEX_ENTITY_HPP