# falling_shapes

simulating the physics of convex shapes when thrown around inside a displaying window
The physics formula come from these two lectures from Ultrecht University,
the first one on [collision detection](https://perso.liris.cnrs.fr/nicolas.pronost/UUCourses/GamePhysics/lectures/lecture%206%20Collision%20Detection.pdf)
and the second one on [collision resolution](https://perso.liris.cnrs.fr/nicolas.pronost/UUCourses/GamePhysics/lectures/lecture%207%20Collision%20Resolution.pdf).
The only extra help I got was from this [post](https://dyn4j.org/2011/11/contact-points-using-clipping/) from the dyn4j physics engine explaining
the contact points resolution using a clippin method that I have implemented using this article.

The collision detection algorithm used is the SAT (Separated Axis Theorem)
and [video examples](/images/bernoulli_better.webm) of the physics engine can be found in the image folder. The fps lag spikes in the videos are not representative of the performances of the 
program as it is due to sfml's way of recording the application window and has nothing to do with the program itself.

The way it, briefly, works is, for each polygon wanting to move,
I first compute the swept bounds according to what the velocity vector is 
and then I run the collision tests on those swept bounds and resolve the collision
before actually moving the polygons. That limits the glitching of the polygons through 
other polygons / walls.

The physics game loop is in the PhysicsEngine.cxx file in the update function.

## polyMaker

This is a subproject of the falling_shapes project, it is used to create convex shapes
and circles and then export it to the physics engine falling_shapes.


## installing and running 

### Linux 

you just have to have the x11 and other graphics library installed, see [SFML manual](https://www.sfml-dev.org/tutorials/2.6/compile-with-cmake.php) on which dependencies are required.

Then you can type `make bouncing_shapes` to compile the physics engine and 
`make poly_maker` to compile the polyMaker program.

To run the polymaker program, you need to be inside the polymaker folder 
and to run the physics engine, you need to be in the falling_shapes folder.

### Non-Linux OS

Unfortuneatly I didn't do any support for compiling on windows or mac OS but
there are tools to install sfml and to convert the Makefile into a CMakeLists.txt
if you have the motivation to do so.

### Remarks

Some of the files are a work in progress but it should compile and work.
The only thing that might not work is when you want to add a textured asset in the polymaker and then import it in the physics engine.