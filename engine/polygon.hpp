#ifndef POLYGON_HPP
#define POLYGON_HPP
#include "physical_object.hpp"


//Polygon class representing any convex polygon
//this polygon is static, that is once the points are set, you can't move them around to modify the polygon's shape
//also the position of a polygon is defined by the position of its centroid in space
class Polygon : public PhysicalObject
{
public:

    //Polygon with 0 points
    //you can create a random polygon manually using the addPoint method
    Polygon();
    //it will create a regular polygon with a certain number of points (3 -> triangle, 4 -> square, 5 -> pentagon, ...) 
    Polygon(float circumscribed_circle_radius, unsigned int regular_number_of_points = 3, sf::Color color = sf::Color::White, float mass = 0.f);
    Polygon(const Polygon& poly);
    
    virtual ~Polygon() override;

    //if you want to create your own polygon
    void setPolygon(const std::vector<sf::Vector2f>& points);

    //only used for regular polygon
    float getSideLength() const;

    //color setter overriden because the points' color needs to be modified
    void setColor(sf::Color new_color) override;

    const sf::VertexArray& getPoints() const;

    //the resulting overlap data (if they collide) is stored in overlap_data
    bool doesCollide(PhysicalObject& other, OverlapData& overlap_data) override;

    //needs to be called before moving the object
    void computeSweptBounds(sf::Vector2f movedOffset) override;

    bool containsPoint(const sf::Vector2f& point);

    //to call only when initial inertia has been modified, for example a change in mass.
    void computeInertia() override;

    PhysicalObjectType getObjectType() const override;

    PhysicalObject* clonePhysicalObject() const override;

	float computeSignedArea();

    void computeCentroid();

    std::vector<sf::Vector2f> getPointsWithoutTranform() const;

    //points in global coordinates
    std::vector<sf::Vector2f> transformedPoints();
    std::vector<sf::Vector2f> transformedSweptBounds();

    sf::VertexArray getTransformedVertices() const;
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    sf::VertexArray fSweptBounds;

    sf::VertexArray fPoints; 
    //used by the containsPoint method
    float farthestDistanceFromCentroid; //the distance between the farthest point from the centroid and the centroid

    float computeTriangleInertia(const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3);

    //backend function to test collision and compute contact point
    //with another polygon using SAT algorithm
    bool doesCollideWithPoly(Polygon& other, OverlapData& overlap_data);

    void computeContactPoints(Polygon& other, OverlapData& overlap_data);

    //for debug purposes only
    sf::VertexArray fContactPointVisual;
    sf::VertexArray fMainFeatures;

    struct FeatureEdge bestFeature(const sf::Vector2f& normal, 
        const std::vector<sf::Vector2f>& points, bool other);
public:

    static float computeSignedArea(const std::vector<sf::Vector2f>& points)
    {
        float area = 0.f;
        for(unsigned int i = 0;i < points.size();++i)
        {
            area += points[i].x * points[(i+1)%points.size()].y
                - points[(i+1)%points.size()].x * points[i].y;
        }
        area *= 0.5f;

        return area;
    }
    static sf::Vector2f computeCentroid(const std::vector<sf::Vector2f>& points)
    {
        sf::Vector2f centroid(0.f,0.f);
        float area = computeSignedArea(points);
        if(std::abs(area) <= __FLT_EPSILON__) return centroid;
        float product_diff = 0.f;

        for(unsigned int i = 0;i<points.size();++i)
        {
            product_diff = points[i].x * points[(i+1)%points.size()].y 
                - points[(i+1)%points.size()].x * points[i].y;
            centroid.x += (points[i].x + points[(i+1)%points.size()].x) * product_diff;
            centroid.y += (points[i].y + points[(i+1)%points.size()].y) * product_diff;
        }
        centroid /= (6.f * area);

        return centroid;
    }
};

#endif //POLYGON_HPP