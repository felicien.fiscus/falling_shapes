#ifndef RESOURCE_IDENTIFIERS_H
#define RESOURCE_IDENTIFIERS_H
#include "resource_holder.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"

#define WIDTH 1500
#define HEIGHT 900
#define TEXTURE_DIR "../resources/textures/"


namespace ObjectType
{
	enum ID
	{
    	None = 0, SimplePolygon = 1, SimpleCircle = 1 << 1, SimpleSprite = 1 << 2,
		 SimplePhysicalObject = SimplePolygon | SimpleCircle | SimpleSprite 
	};
};

namespace Textures
{
	enum ID
	{
		Button, TexturesCount //textures
	};
};

namespace Fonts
{
	enum ID
	{
		Stats
	};
}

namespace Sounds
{
	enum ID
	{
		Button //Sounds
	};
}

typedef ResourceHolder<sf::Texture, Textures::ID> TextureHolder;
typedef ResourceHolder<sf::Font, Fonts::ID> FontHolder;
typedef ResourceHolder<sf::SoundBuffer, Sounds::ID> SoundBufferHolder;

#endif //RESOURCE_IDENTIFIERS_H