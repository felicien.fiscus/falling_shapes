

numberOfDigits=$1

if [ $numberOfDigits == 3 ] ; then
    for i in `seq 0 9`; do
        mv image_$i.png image_00$i.png
    done

    for i in `seq 10 99`; do
        mv image_$i.png image_0$i.png
    done
fi

if [ $numberOfDigits == 4 ] ; then
    for i in `seq 0 9`; do
        mv image_$i.png image_000$i.png
    done

    for i in `seq 10 99`; do
        mv image_$i.png image_00$i.png
    done

    for i in `seq 100 999`; do
        mv image_$i.png image_0$i.png
    done

fi

ffmpeg -framerate 60 -i image_%0${numberOfDigits}d.png $2.webm