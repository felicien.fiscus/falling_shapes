#ifndef RESOURCE_HOLDER_H
#define RESOURCE_HOLDER_H
#include <string>
#include <map>

template<typename Resource, typename ID>
class ResourceHolder
{
	struct mapResource
	{
		mapResource()
			:resource(nullptr), fileName(""){}
		mapResource(Resource* _resource, const std::string& file_name)
			:resource(_resource), fileName(file_name){}
		Resource* resource;
		std::string fileName;
	};
	std::map<ID, mapResource> fResources;
public:
	ResourceHolder();
	virtual ~ResourceHolder();

	void load(const std::string& FileName, ID id);

	template<typename Param>
	void load(const std::string& FileName, ID id, Param SecondParam);

	Resource& get(ID id);
	const Resource& get(ID id) const;

	size_t getNumberOfResourceLoaded() const;

	const std::string& getResourceFileName(ID id) const;

	//returns nullptr if the resource is new and the associated resource if it's already loaded
	Resource* isResourceFileAlreadyLoaded(const std::string& fileName) const;

};


#include "resource_holder.inl"
#endif //RESOURCE_HOLDER_H