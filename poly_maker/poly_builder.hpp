#ifndef POLY_BUILDER_HPP
#define POLY_BUILDER_HPP
#include <list>
#include "importable_object.hpp"
#include "menu.hpp"

class PolyBuilder : public sf::Drawable
{
public:

    struct PolyPair
    {
        sf::VertexArray* poly = nullptr;
        ImportableObject* importableObject = nullptr;   
        

        bool operator==(const PolyPair& other)
        {
            return other.poly == poly && importableObject == other.importableObject;
        }
    };

    PolyBuilder(const sf::RenderWindow& context_window, const TextureHolder& textures, const FontHolder& fonts);
    ~PolyBuilder();

    void update(sf::Time dt);
    void handleEvent(const sf::Event& e);

    /*
    *   exports the data of the different polygons built on the screens as a json outputfile
    */
    void exportPolygons(const std::string& outputFile);

    void addPolyPair(const PolyPair& poly, bool isBeingConstructed = false);

    static bool doesPolygonContainsPoint(const sf::Vector2f& point, const sf::VertexArray& poly);
    
    void movePolygons(const sf::Vector2f& offset);

    bool isShowingMenu() const;
private:

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    const sf::RenderWindow& contextWindow;

    std::list<PolyPair> polygons;
    //whether the next time we click corresponds to the first vertex of the polygon we're going to build
    bool firstPoint = true;
    bool isControlKeyPressed = false;
    //the edge that is going to be added next
    sf::VertexArray currentLinePlacement;
    PolyPair polygonInConstruction;
    //used to determine the inside/outside of the polygon we're building
    int insideOutsideSign = 1;
    sf::Color currentColor = sf::Color::Blue;
    PolyPair attachedPolygon;
    sf::Vector2f previousMousePosition;

    sf::RectangleShape menuOptionsBackground;
    myGUI::Menu frontPolyMenuOptions;
    const TextureHolder& textures;
    const FontHolder& fonts;
    void buildFrontPolyMenuOptions();

    void setColorLatestPoly(sf::Color color);

    void addVertexToCurrentPolygon();
    void deleteCurrentPolygon();
    void copyPolygon(const PolyPair& poly);
    PolyPair getPolygonContainingCursor() const;
};


#endif //POLY_BUILDER_HPP