#ifndef BUTTON_H
#define BUTTON_H
#include "component.hpp"


namespace myGUI
{
	/*
	Button class, a button that can be clicked and performs an action
	when clicked on
	
	**/
	class Button : public Component
	{
	public:
		//callback is the action that is to be perform when cliked and the 'text' 
		//will be inside of the button. If you don't want to render any text in the button,
		//just put "" as a string for 'text' and there will be no draw call for it
		Button(const sf::Texture& button_texture, const sf::Font& button_font, const std::string& text, bool can_toggle = true);
		~Button();

		void update(sf::Time) override;
		void handleEvent(const sf::Event& e);
		ComponentType getType() const;
		
		void click() override;
		void hover(bool) override;

		bool isToggled() const;
		bool canBeToggled() const;
		
		//if you want to manually set the texture position and sizes of each button state
		void setTexRect(sf::IntRect normalTexRect, sf::IntRect hoverTexRect, sf::IntRect clickedTexRect = sf::IntRect());
		
		//the bounding rectangle has its coordinates system based on the window it is in
		sf::FloatRect getBoundingRect() const override;

		void setText(const std::string& text);
	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		//different textures positions
		sf::IntRect fNormalTexRect;
		sf::IntRect fHoverTexRect;
		sf::IntRect fClickedTexRect; //optional
		bool toggle = false;
		bool canToggle;

		//resources
		sf::Sprite fButtonSprite;
		sf::Text fTextInButton;
		sf::Sound fClickSound;

	};

}

#endif //BUTTON_H

