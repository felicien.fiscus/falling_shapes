#include "application.hpp"
#include "../maths_operations.hpp"
#include "button.hpp"
#include <numeric>
#include <fstream>
#include "stdio.h"
#include "text_box.hpp"
#include "importable_physical_sprite.hpp"
#include <error.h>

namespace textBoxIndices
{
    enum importSprite
    {
        file_name, entity_type, rect_left, rect_top, rect_width, rect_height, 
    };
}

Application::Application()
    :currentState(State::None), fWindow(),
    fFrameRate(sf::seconds(1.f/60.f))
    , fRealFrameRateText()
    , fFonts()
    , fTextures()
    , menuBackground()
    , fMenu(fWindow)
    , sprites()
    , polyBuilder(fWindow, fTextures, fFonts)
    , circleBuilder(fWindow, fTextures, fFonts)
{
    loadFonts();
    loadTextures();

    fRealFrameRateText.setFont(fFonts.get(Fonts::ID::Stats));
    fRealFrameRateText.setCharacterSize(10U);
    fRealFrameRateText.setPosition(5.f, 5.f);
    
    buildMenu();

    fWindow.create(sf::VideoMode(WIDTH + fMenu.getBoundingRect().width, HEIGHT), "Poly Maker", sf::Style::Close);
    menuBackground.setSize(sf::Vector2f(fMenu.getBoundingRect().width, HEIGHT));
    menuBackground.setPosition(WIDTH, 0.f);
    menuBackground.setFillColor(sf::Color(255, 200, 152));
    fMenu.setPosition(WIDTH, 0.f);
}

Application::~Application()
{
    for(auto* sprite : sprites)
        delete sprite;
}


void Application::loadTextures()
{
    fTextures.load(TEXTURE_DIR + std::string("buttons.png"), Textures::ID::Button);
}


void Application::buildMenu()
{
    buildPolyBuilderButton();
    buildCircleBuilderButton();
    buildImportButton();
    buildExportButton();
    buildImportSpriteTextBox();
}


void Application::buildCircleBuilderButton()
{
    using namespace myGUI;

    Button* circleBuilderButton = new Button(fTextures.get(Textures::Button), fFonts.get(Fonts::Stats), "circle");
    circleBuilderButton->fCallback = [this, circleBuilderButton]()
    {
        for(auto* comp : fMenu)
        {
            
            if(comp->getType() == ComponentType::Button)
            {
                Button* button = static_cast<Button*>(comp);
                if(button && button != circleBuilderButton && button->isToggled())
                {
                    button->click();
                }
            }
        }

        if(!circleBuilderButton->isToggled())
            currentState = Application::State::BuildCircle;
        else
            currentState = Application::State::None;

    };
    
    circleBuilderButtonRef = circleBuilderButton;
    fMenu.addComp(circleBuilderButton);

}
void Application::buildPolyBuilderButton()
{
    using namespace myGUI;

    Button* polyBuilderButton = new Button(fTextures.get(Textures::Button), fFonts.get(Fonts::Stats), "polygon");

    polyBuilderButton->fCallback = [this, polyBuilderButton]()
    {
        for(auto* comp : fMenu)
        {
            if(comp->getType() == ComponentType::Button)
            {
                Button* button = static_cast<Button*>(comp);
                if(button && button != polyBuilderButton && button->isToggled())
                {
                    button->click();
                }
            }
        }

        if(!polyBuilderButton->isToggled())
            currentState = Application::State::BuildPolygon;
        else
            currentState = Application::State::None;

    };

    polyBuilderButtonRef = polyBuilderButton;
    polyBuilderButton->setPosition(10.f, 10.f);

    fMenu.addComp(polyBuilderButton);

}
void Application::buildExportButton()
{
    using namespace myGUI;

    Button* exportButton = new Button(fTextures.get(Textures::Button), fFonts.get(Fonts::Stats), "export", false);
    exportButton->fCallback = [this, exportButton]()
    {
        for(auto* comp : fMenu)
        {
            if(comp->getType() == ComponentType::Button)
            {
                Button* button = static_cast<Button*>(comp);
                if(button && button != exportButton && button->isToggled())
                {
                    button->click();
                }
            }
        }

        polyBuilder.exportPolygons("custom_shapes.json");
        circleBuilder.exportCircles("custom_shapes.json");
        exportSprites("custom_shapes.json");
    };

    fMenu.addComp(exportButton);

}

void Application::exportSprites(const std::string& file)
{
    nlohmann::json polyJSON;
    //first get the potential json already in the file
    std::ifstream in_file{file};
    if(in_file.is_open())
    {
        in_file >> polyJSON;
    }

    for(auto* sprite : sprites)
    {
        polyJSON.push_back(nlohmann::json());
        nlohmann::json& lastAdded = polyJSON[polyJSON.size() - 1];

        sprite->exportObjectData(lastAdded);
    }

    //then dump it in a json file
    std::ofstream out_file{file};
    if(!out_file.is_open())
        throw std::runtime_error("unable to open file: " + file);

    out_file << std::setw(4) << polyJSON << "\n]" << std::endl;
    
    out_file.close();
}

void Application::buildImportButton()
{
    using namespace myGUI;

    Button* importButton = new Button(fTextures.get(Textures::Button), fFonts.get(Fonts::Stats), "import", false);
    importButton->fCallback = [this, importButton]()
    {
        for(auto* comp : fMenu)
        {
            if(comp->getType() == ComponentType::Button)
            {
                Button* button = static_cast<Button*>(comp);
                if(button && button != importButton && button->isToggled())
                {
                    button->click();
                }
            }
        }
        importShapes("custom_shapes.json");
    };

    fMenu.addComp(importButton);

}

void Application::buildImportSpriteTextBox()
{
    using namespace myGUI;

    TextBox* importSpriteButton = new TextBox(fWindow, "import sprite", fTextures.get(Textures::Button), 
        fFonts.get(Fonts::Stats), 20U);
    importSpriteButton->setSubmitButtonCallback([](){std::cout<<"callback !"<<std::endl;});
    importSpriteButton->setSubmitButtonText("import");
    importSpriteButton->fCallback =
    [this]()
    {
        for(auto* comp : fMenu)
        {
            if(comp->getType() == ComponentType::Button)
            {
                Button* button = static_cast<Button*>(comp);
                if(button && button->isToggled())
                {
                    button->click();
                }
            }
        }
    };

    (void)importSpriteButton->addTextBox("entity type", 15U, TextBox::Down);
    (void)importSpriteButton->addTextBox("left", 10U, TextBox::Down);
    (void)importSpriteButton->addTextBox("top", 10U, TextBox::Down);
    (void)importSpriteButton->addTextBox("width", 10U, TextBox::Down);
    (void)importSpriteButton->addTextBox("height", 10U, TextBox::Down);

    importSpriteButton->setSubmitButtonCallback(
        [importSpriteButton, this]()
        {

            ObjectType::ID entityType = ObjectType::SimplePhysicalObject;
            //extract the left, top, width and height from the text box inputs
            int left = 0, top = 0, width = 0, height = 0;
            left = sscanf(importSpriteButton->getInsideText(textBoxIndices::rect_left).c_str(), "%i", &left) == EOF ? 0 : left;
            top = sscanf(importSpriteButton->getInsideText(textBoxIndices::rect_top).c_str(), "%i", &top) == EOF ? 0 : top;
            width = sscanf(importSpriteButton->getInsideText(textBoxIndices::rect_width).c_str(), "%i", &width) == EOF ? -1 : width;
            height = sscanf(importSpriteButton->getInsideText(textBoxIndices::rect_height).c_str(), "%i", &height) == EOF ? -1 : height;

            importAccordingToType(entityType, importSpriteButton, sf::IntRect(left, top, width, height));
        }
    );

    fMenu.addComp(importSpriteButton);
}

void Application::importAccordingToType(ObjectType::ID type, myGUI::TextBox* importSpriteTextBox, const sf::IntRect& texrect)
{
    switch (type)
    {
    case ObjectType::SimplePhysicalObject:
        if(importSpriteTextBox->getInsideText(textBoxIndices::file_name) == "")
            return;
        importSimplePhysicalObjectSprite(importSpriteTextBox, texrect);
        break;
    default:
        break;
    }
}

void Application::importSimplePhysicalObjectSprite(myGUI::TextBox *importSpriteButton, const sf::IntRect &textureRect)
{
    std::string fileName = importSpriteButton->getInsideText(textBoxIndices::file_name);
    char boundsType = fileName[0];
    fileName = fileName.substr(1) + ".png";
    sf::Texture* texture = nullptr;
    sf::IntRect texRect(textureRect);
    ImportablePhysicalSprite* new_sprite = new ImportablePhysicalSprite(fTextures);    

    //test if we already loaded the texture from the specified file
    if(!(texture = fTextures.isResourceFileAlreadyLoaded(TEXTURE_DIR + fileName)))
    {
        fTextures.load(TEXTURE_DIR + fileName, (Textures::ID)(++numberOfSpriteImported));
        texture = &fTextures.get((Textures::ID)numberOfSpriteImported);
    }
    if(texRect.width == -1)
        texRect.width = texture->getSize().x;
    if(texRect.height == -1)
        texRect.height = texture->getSize().y;
        
    new_sprite->sprite.setTexture(*texture);
    new_sprite->sprite.setTextureRect(texRect);
    new_sprite->setPosition(WIDTH * 0.5f, HEIGHT * 0.5f);
    new_sprite->textureFileName = fileName;

    switch (boundsType)
    {
    case 'c':   //that means the texture corresponds to a circle bound
    {   
        CircleBuilder::CirclePair circle;
        circle.importableObject = new_sprite;

        circleBuilder.addCirclePair(circle, true);
        circleBuilderButtonRef->click();
    }
        break;
    case 'p':   //that means the texture corresponds to a polygon bound
    {   
        PolyBuilder::PolyPair poly; 
        poly.importableObject = new_sprite;

        polyBuilder.addPolyPair(poly, true);
        polyBuilderButtonRef->click();
    }
        break;
    default:
        std::cerr<<"for simple PhysicalObject you need to prefix the filename with either 'c'(for circle) or 'p'(for polygon)"<<std::endl;
        break;
    }

}

void Application::importCircle(const nlohmann::json& shape_data)
{
    CircleBuilder::CirclePair circle;
    circle.circle = new sf::CircleShape(shape_data["radius"]);
    circle.circle->setFillColor(sf::Color::Transparent);
    circle.circle->setOutlineColor((shape_data["isStatic"]) ? sf::Color::Red : sf::Color::Blue);
    circle.circle->setOutlineThickness(1.f);
    circle.circle->setOrigin(shape_data["radius"], shape_data["radius"]);
    circle.circle->setPosition(shape_data["boundsPosition"][0], shape_data["boundsPosition"][1]);

    circle.importableObject = ImportableObject::getInstanceFromObjectType((ObjectType::ID)shape_data["type"], fTextures);
    circle.importableObject->setOrigin(shape_data["origin"][0], shape_data["origin"][1]);
    sf::Vector2f absolutePosition(shape_data["position"][0], shape_data["position"][1]);
    //because of the way it is exported, see circleBuilder::exportCircles()
    absolutePosition += circle.circle->getPosition() - circle.circle->getOrigin();
    circle.importableObject->setPosition(absolutePosition);
    circle.importableObject->setScale(shape_data["scale"][0], shape_data["scale"][1]);
    circle.importableObject->setRotation(shape_data["rotation"]);
    circle.importableObject->initImportableObjectFromJSON(shape_data);

    circleBuilder.addCirclePair(circle);

}
void Application::importPolygon(const nlohmann::json& shape_data)
{
    PolyBuilder::PolyPair poly;
    poly.importableObject = ImportableObject::getInstanceFromObjectType((ObjectType::ID)shape_data["type"], fTextures);
    sf::VertexArray* points = new sf::VertexArray(sf::PrimitiveType::LineStrip);
    poly.importableObject->setOrigin(shape_data["origin"][0], shape_data["origin"][1]);
    poly.importableObject->setPosition(shape_data["position"][0], shape_data["position"][1]);
    poly.importableObject->setScale(shape_data["scale"][0], shape_data["scale"][1]);
    poly.importableObject->setRotation(shape_data["rotation"]);
    poly.importableObject->initImportableObjectFromJSON(shape_data);
    sf::Color points_color = (shape_data["isStatic"]) ? sf::Color::Red : sf::Color::Blue;

    for(auto& coords_pair : shape_data["poly_vertices"])
    {
        points->append(sf::Vector2f(coords_pair[0], coords_pair[1]));
        (*points)[points->getVertexCount() - 1].color = points_color;
    }
    points->append((*points)[0]);
    poly.poly = points;

    polyBuilder.addPolyPair(poly);

}

void Application::importShapes(const std::string& input_file)
{
    std::ifstream input{input_file};
    if(!input.is_open())
        throw std::runtime_error("unable to open file: " + input_file);

    nlohmann::json jsonShapes;

    input >> jsonShapes;

    input.close();
    
    for(auto& shape : jsonShapes)
    {
        if(shape["boundsType"] == "circle")
        {
            importCircle(shape);
        }
        else if(shape["boundsType"] == "polygon")
        {
            importPolygon(shape);
        }
        else
        {
            ImportableObject* new_sprite = ImportableObject::getInstanceFromObjectType((ObjectType::ID)shape["type"], fTextures);   
            new_sprite->initImportableObjectFromJSON(shape);

            sprites.push_front(new_sprite);
        }
    }
}

void Application::run()
{
    sf::Clock appClock;
    sf::Time accu = sf::Time::Zero, deltaTime = sf::Time::Zero;
    sf::Time realFrameRateAccu = sf::Time::Zero;

    while(fWindow.isOpen())
    {
        deltaTime = appClock.restart();
        accu += deltaTime;
        realFrameRateAccu += deltaTime;
        //update the application
        while(accu >= fFrameRate)
        {
            accu -= fFrameRate;

            handleEvent();
            update();

        } 

        //update the real frame rate displaying text
        if(realFrameRateAccu >= sf::seconds(0.5f))
        {
            realFrameRateAccu = sf::Time::Zero;

            fRealFrameRateText.setString(std::to_string((int)(1 / deltaTime.asSeconds())) + " fps\n" + std::to_string(deltaTime.asMicroseconds()) + "us");
        }
        
        render();
    }
}


void Application::moveEverythingBy(const sf::Vector2f& offset)
{
    polyBuilder.movePolygons(offset);
    circleBuilder.moveCircles(offset);
}

void Application::keyEvents(const sf::Event& event)
{
    if(currentSpriteAttached)
        return;
    sf::Vector2f mouse_pos(sf::Mouse::getPosition(fWindow));
    switch (event.key.code)
    {
    case sf::Keyboard::Up:
    moveEverythingBy(sf::Vector2f(0.f, -5.f));
    break;
    case sf::Keyboard::Down:
    moveEverythingBy(sf::Vector2f(0.f, 5.f));
    break;
    case sf::Keyboard::Left:
    moveEverythingBy(sf::Vector2f(-5.f, 0.f));
    break;
    case sf::Keyboard::Right:
    moveEverythingBy(sf::Vector2f(5.f, 0.f));
    break;
/*     case sf::Keyboard::P:
        if(shiftPressed)
            polyBuilderButtonRef->click();
    break;
    case sf::Keyboard::C:
        if(shiftPressed)
            circleBuilderButtonRef->click();
    break; */
    case sf::Keyboard::LShift:
        shiftPressed = true;
        break;
    default:
        break;
    }

}



void Application::handleEvent()
{
    sf::Event event;
    while(fWindow.pollEvent(event))
    {

        switch (currentState)
        {
        case State::BuildPolygon:
            polyBuilder.handleEvent(event);
            break;
        case State::BuildCircle:
            circleBuilder.handleEvent(event);
            break;
        case State::None:
            handleNoneStateEvents(event);
            break;
        
        default:
            break;
        }

        switch (event.type)
        {
        case sf::Event::Closed:
            fWindow.close();
            break;
        case sf::Event::KeyPressed:
            keyEvents(event);
        case sf::Event::KeyReleased:
            if(event.key.code == sf::Keyboard::LShift)
                shiftPressed = false;
            break;
        default:
            break;
        }

        fMenu.handleEvent(event);



    }

}

ImportableObject* Application::doesSpriteContainsPoint(const sf::Vector2f& point)
{
    for(auto* sprite : sprites)
    {
        if(sprite->getBoundingRect().contains(point))
        {
            return sprite;
        }
    }

    return nullptr;
}

void Application::handleNoneStateEvents(const sf::Event& e)
{
    switch (e.type)
    {
    case sf::Event::MouseButtonPressed:
        if(currentSpriteAttached)
        {
            currentSpriteAttached = nullptr;
        }
        else
        {
            sf::Vector2f mousePosition(sf::Mouse::getPosition(fWindow));
            if((currentSpriteAttached = doesSpriteContainsPoint(mousePosition)))
            {
                currentSpriteAttached->setPosition(mousePosition);
            }
        }
        break;
    case sf::Event::MouseMoved:
        if(currentSpriteAttached)
        {
            currentSpriteAttached->setPosition(sf::Vector2f(sf::Mouse::getPosition(fWindow)));
        }
        break;
    case sf::Event::KeyPressed:
        if(currentSpriteAttached)
        {
            switch (e.key.code)
            {
            case sf::Keyboard::Left:
                currentSpriteAttached->rotate(5.f);
                break;
            case sf::Keyboard::Right:
                currentSpriteAttached->rotate(-5.f);
                break;
            case sf::Keyboard::Up:
                currentSpriteAttached->scale(sf::Vector2f(1.1f, 1.1f));
                break;
            case sf::Keyboard::Down:
                currentSpriteAttached->scale(sf::Vector2f(0.9f, 0.9f));
                break;
            
            default:
                break;
            }
        }
        break;
    default:
        break;
    }
}
void Application::updateNoneState(sf::Time dt)
{

}

void Application::update()
{
    fMenu.update(fFrameRate);
    switch (currentState)
    {
    case State::BuildPolygon:
        polyBuilder.update(fFrameRate);
        break;
    case State::BuildCircle:
        circleBuilder.update(fFrameRate);
        break;
    case State::None:
        updateNoneState(fFrameRate);
        break;
    
    default:
        break;
    }
}

void Application::render()
{
    fWindow.clear();

    for(auto* sprite : sprites)
        fWindow.draw(*sprite);

    if(circleBuilder.isShowingMenu())
    {
        fWindow.draw(polyBuilder);
        fWindow.draw(circleBuilder);
    }
    else
    {
        fWindow.draw(circleBuilder);
        fWindow.draw(polyBuilder);
    }

    fWindow.draw(menuBackground);
    fWindow.draw(fMenu);
    
    fWindow.draw(fRealFrameRateText);
    fWindow.display();
    
}

void Application::loadFonts()
{
    fFonts.load("../resources/fonts/calibri.ttf", Fonts::ID::Stats);
}