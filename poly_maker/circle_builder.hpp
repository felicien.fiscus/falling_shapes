#ifndef CIRCLE_BUILDER_HPP
#define CIRCLE_BUILDER_HPP
#include <list>
#include "importable_object.hpp"
#include "menu.hpp"

class CircleBuilder : public sf::Drawable
{
public:
    struct CirclePair
    {
        sf::CircleShape* circle = nullptr;
        ImportableObject* importableObject = nullptr;
        
        bool operator==(const CirclePair& other)
        {
            return other.circle == circle && importableObject == other.importableObject;
        }
    };

    CircleBuilder(const sf::RenderWindow& context_window, const TextureHolder& textures, const FontHolder& fonts);
    ~CircleBuilder();

    void update(sf::Time dt);
    void handleEvent(const sf::Event& e);

    void exportCircles(const std::string& outputFile);

    void addCirclePair(const CirclePair& circle, bool isBeingConstructed = false);

    void moveCircles(const sf::Vector2f& offset);
    
    bool isShowingMenu() const;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    const sf::RenderWindow& contextWindow;

    std::list<CirclePair> circles;
    bool buttonPressed = false;
    bool controlKeyPressed = false;
    sf::Vector2f mousePositionToCreateCircle;
    sf::Color currentColor = sf::Color::Blue;
    CirclePair attachedCircle;
    sf::Vector2f previousMousePosition; //used to move the attached circle and its sprite around

    sf::RectangleShape menuOptionsBackground;
    myGUI::Menu objectMenuOptions;
    const TextureHolder& textures;
    const FontHolder& fonts;
    void buildFrontCircleOptionMenu();

    void createCircle();
    void expandCircle(float amount);
    void deleteCircle(CirclePair circle);
    void copyCircle(const CirclePair& circle);
    CirclePair findCircleContainingMouse() const;
};

#endif //CIRCLE_BUILDER_HPP