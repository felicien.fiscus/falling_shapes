#include "button.hpp"
#include "../../maths_operations.hpp"
#include "menu.hpp"


namespace myGUI
{
	Button::Button(const sf::Texture& button_texture, const sf::Font& button_font, const std::string& text, bool can_toggle)
		:Component(), fNormalTexRect(0, 0, 80, 60), fHoverTexRect(80, 0, 80, 60), fClickedTexRect(160, 0, 80, 60), canToggle(can_toggle),
		fButtonSprite(button_texture, fNormalTexRect),
		fTextInButton(text, button_font), fClickSound()
	{
		if (text != "")
		{
			fTextInButton.setOrigin(fTextInButton.getLocalBounds().left + fTextInButton.getLocalBounds().width / 2.f, fTextInButton.getLocalBounds().top + fTextInButton.getLocalBounds().height / 2.f);
			sf::Vector2f button_wh(fButtonSprite.getLocalBounds().width, fButtonSprite.getLocalBounds().height);
			fTextInButton.setPosition(button_wh.x * 0.5f, button_wh.y * 0.5f);
		}
	}

	Button::~Button()
	{
	}

	void Button::update(sf::Time dt)
	{
		if(isHovering() && sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{	
			fButtonSprite.setTextureRect(fClickedTexRect);	
		}
	}

	ComponentType Button::getType() const
	{
		return ComponentType::Button;
	}

	void Button::handleEvent(const sf::Event& e)
	{
		switch (e.type)
		{
		case sf::Event::MouseButtonReleased:
			if(!canToggle && toggle)
			{
				toggle = false;
				fButtonSprite.setTextureRect(fNormalTexRect);
			}
			break;
		
		default:
			break;
		}
	}

	bool Button::canBeToggled() const
	{
		return canToggle;
	}


	bool Button::isToggled() const
	{
		return toggle;
	}

	void Button::click()
	{
		Component::click();
		toggle = !toggle;
		if(toggle)
			fButtonSprite.setTextureRect(fClickedTexRect);
		else
			fButtonSprite.setTextureRect(fNormalTexRect);
		//fClickSound.play();
	}

	void Button::hover(bool flag)
	{
		Component::hover(flag);
		if(toggle)
			return;
			
		if (fIsHovering)
		{
			fButtonSprite.setTextureRect(fHoverTexRect);
		}
		else
		{
			fButtonSprite.setTextureRect(fNormalTexRect);
		}
	}

	void Button::setTexRect(sf::IntRect normalTexRect, sf::IntRect hoverTexRect, sf::IntRect clickedTexRect)
	{
		fNormalTexRect = normalTexRect;
		fHoverTexRect = hoverTexRect;
		fClickedTexRect = clickedTexRect;
		//don't forget to update the sprite
		hover(isHovering());
	}

	sf::FloatRect Button::getBoundingRect() const
	{
		if(!parentMenu)
			return getTransform().transformRect(fButtonSprite.getGlobalBounds());
			
		return parentMenu->getTransform().transformRect(getTransform().transformRect(fButtonSprite.getGlobalBounds()));
	}

	void Button::setText(const std::string& text)
	{
		fTextInButton.setString(text);
	}

	void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		target.draw(fButtonSprite, states);
		if (fTextInButton.getString() != "")
		{
			target.draw(fTextInButton, states);
		}
	}
}