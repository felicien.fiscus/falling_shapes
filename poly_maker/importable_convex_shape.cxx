#include "importable_convex_shape.hpp"
#include "text_box.hpp"
#include "menu.hpp"


ImportableConvexShape::ImportableConvexShape()
    :ImportableObject(), shape()
{

}

ImportableConvexShape::~ImportableConvexShape()
{

}


ImportableObject* ImportableConvexShape::cloneImportableObject() 
{
    return new ImportableConvexShape(*this);
}

ImportableObject* ImportableConvexShape::initImportableObjectFromJSON(const nlohmann::json& object_data) 
{
    shape.setFillColor(sf::Color(object_data["FillColor"]));
    shape.setOutlineColor(sf::Color(object_data["OutlineColor"]));
    shape.setOutlineThickness(object_data["OutlineThickness"]);
    setOrigin(object_data["origin"][0], object_data["origin"][1]);
    setPosition(object_data["position"][0], object_data["position"][1]);
    setScale(object_data["scale"][0], object_data["scale"][1]);
    setRotation(object_data["rotation"]);
    shape.setPointCount(object_data["poly_vertices"].size());
    isStatic = object_data["isStatic"];
    mass = object_data["mass"];
    size_t i  = 0;
    for(auto& coord_pair : object_data["shape_vertices"])
    {
        shape.setPoint(i++, sf::Vector2f(coord_pair[0], coord_pair[1]));
    }

    return this;
}


myGUI::TextBox* ImportableConvexShape::createImportableObjectMenuOptions(const sf::RenderWindow& contextWindow,
                                                            const TextureHolder& textures, 
                                                            const FontHolder& fonts) 
{
    using namespace myGUI;

    enum class convexShape_boxes
    {
        fill_color, outline_color, outline_thickness, mass, is_static
    };

    TextBox* textBoxes = new TextBox(contextWindow, "fill color", textures.get(Textures::Button), fonts.get(Fonts::Stats), 15U);
    textBoxes->addTextBox("outline color", 15U);
    textBoxes->addTextBox("outline thickness", 5U);
    textBoxes->addTextBox("mass", 5U);
    textBoxes->addTextBox("static (y/n)", 2U);

    textBoxes->setSubmitButtonText("apply");
    textBoxes->setSubmitButtonCallback(
        [this, textBoxes]()
        {
            std::string fillColor_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::fill_color);
            std::string outlineColor_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::outline_color);
            std::string outlineThickness_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::outline_thickness);
            std::string mass_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::mass);
            std::string isStatic_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::is_static);
            
            sf::Color fillColor = ColorConverter::fromStringToColor(fillColor_text);
            sf::Color outlineColor = ColorConverter::fromStringToColor(outlineColor_text);
            float outlineThickness = 0.f;

            if(fillColor == sf::Color::Transparent)
                ColorConverter::tryInitColorWithRGB(fillColor, fillColor_text);
            if(outlineColor == sf::Color::Transparent)
                ColorConverter::tryInitColorWithRGB(outlineColor, outlineColor_text);
            outlineThickness = (sscanf(outlineThickness_text.c_str(), "%f", &outlineThickness) == EOF) ? -1.f : outlineThickness;
            mass = (sscanf(mass_text.c_str(), "%f", &mass) == EOF) ? 0.5f : mass;
            if(isStatic_text.size() > 0)
            {
                if(isStatic_text[0] == 'y')
                    isStatic = true;
                else if(isStatic_text[0] == 'n')
                    isStatic = false;
            }
            
            if(fillColor_text != "")
                shape.setFillColor(fillColor);
            if(outlineColor_text != "")
                shape.setOutlineColor(outlineColor);
            if(outlineThickness != -1.f)
                shape.setOutlineThickness(outlineThickness);

            if(textBoxes->parentMenu)
                textBoxes->parentMenu->isMenuClosed = true;
        }
    );

    return textBoxes;
}


void ImportableConvexShape::exportObjectData(nlohmann::json& output_data) 
{
    output_data["FillColor"] = shape.getFillColor().toInteger();
    output_data["OutlineColor"] = shape.getOutlineColor().toInteger();
    output_data["OutlineThickness"] = shape.getOutlineThickness();
    output_data["position"] = {getPosition().x, getPosition().y};
    output_data["origin"] = {getOrigin().x, getOrigin().y};
    output_data["scale"] = {getScale().x, getScale().y};
    output_data["rotation"] = getRotation();

    output_data["shape_vertices"] = {};
    for(unsigned int i = 0;i<shape.getPointCount();++i)
    {
        output_data["shape_vertices"].push_back({shape.getPoint(i).x, shape.getPoint(i).y});
    }

    output_data["isStatic"] = isStatic;
    output_data["mass"] = mass;
    output_data["type"] = getObjectType();
}


ObjectType::ID ImportableConvexShape::getObjectType() const 
{
    return ObjectType::SimplePolygon;
}

sf::FloatRect ImportableConvexShape::getBoundingRect() const 
{
    return getTransform().transformRect(shape.getGlobalBounds());
}

void ImportableConvexShape::setPolygonPoints(const sf::VertexArray& points_array)
{
    shape.setPointCount(points_array.getVertexCount() - 1);
    for(unsigned int i = 0;i<points_array.getVertexCount() - 1;++i)
    {
        shape.setPoint(i, points_array[i].position);
    }
}

void ImportableConvexShape::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    
    target.draw(shape, states);
}