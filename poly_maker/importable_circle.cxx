#include "importable_circle.hpp"
#include "text_box.hpp"
#include "menu.hpp"

ImportableCircle::ImportableCircle()
    :ImportableObject(), circle()
{

}

ImportableCircle::~ImportableCircle()
{

}


//get a copy of the 'this' instance of the object
ImportableObject* ImportableCircle::cloneImportableObject()
{
    return new ImportableCircle(*this);
}


//initializes the content of this object and returns a pointer to this
ImportableObject* ImportableCircle::initImportableObjectFromJSON(const nlohmann::json& object_data)
{
    circle.setFillColor(sf::Color(object_data["FillColor"]));
    circle.setOutlineColor(sf::Color(object_data["OutlineColor"]));
    circle.setOutlineThickness(object_data["OutlineThickness"]);
    circle.setRadius(object_data["radius"]);
    isStatic = object_data["isStatic"];
    mass = object_data["mass"];

    return this;

}


myGUI::TextBox* ImportableCircle::createImportableObjectMenuOptions(const sf::RenderWindow& contextWindow,
                                                            const TextureHolder& textures, 
                                                            const FontHolder& fonts)
{
    using namespace myGUI;

    enum class convexShape_boxes
    {
        fill_color, outline_color, outline_thickness, mass, is_static
    };

    TextBox* textBoxes = new TextBox(contextWindow, "fill color", textures.get(Textures::Button), fonts.get(Fonts::Stats), 15U);
    textBoxes->addTextBox("outline color", 15U);
    textBoxes->addTextBox("outline thickness", 5U);
    textBoxes->addTextBox("mass", 5U);
    textBoxes->addTextBox("static (y/n)", 2U);

    textBoxes->setSubmitButtonText("apply");
    textBoxes->setSubmitButtonCallback(
        [this, textBoxes]()
        {
            std::string fillColor_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::fill_color);
            std::string outlineColor_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::outline_color);
            std::string outlineThickness_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::outline_thickness);
            std::string mass_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::mass);
            std::string isStatic_text = textBoxes->getInsideText((unsigned int)convexShape_boxes::is_static);
            
            sf::Color fillColor = ColorConverter::fromStringToColor(fillColor_text);
            sf::Color outlineColor = ColorConverter::fromStringToColor(outlineColor_text);
            float outlineThickness = 0.f;

            if(fillColor == sf::Color::Transparent)
                ColorConverter::tryInitColorWithRGB(fillColor, fillColor_text);
            if(outlineColor == sf::Color::Transparent)
                ColorConverter::tryInitColorWithRGB(outlineColor, outlineColor_text);
            outlineThickness = (sscanf(outlineThickness_text.c_str(), "%f", &outlineThickness) == EOF) ? -1.f : outlineThickness;
            mass = (sscanf(mass_text.c_str(), "%f", &mass) == EOF) ? 0.5f : mass;
            if(isStatic_text.size() > 0)
            {
                if(isStatic_text[0] == 'y')
                    isStatic = true;
                else if(isStatic_text[0] == 'n')
                    isStatic = false;
            }
            
            if(fillColor_text != "")
                circle.setFillColor(fillColor);
            if(outlineColor_text != "")
                circle.setOutlineColor(outlineColor);
            if(outlineThickness != -1.f)
                circle.setOutlineThickness(outlineThickness);

            if(textBoxes->parentMenu)
                textBoxes->parentMenu->isMenuClosed = true;
        }
    );

    return textBoxes;

}

void ImportableCircle::exportObjectData(nlohmann::json& output_data)
{
    output_data["FillColor"] = circle.getFillColor().toInteger();
    output_data["OutlineColor"] = circle.getOutlineColor().toInteger();
    output_data["OutlineThickness"] = circle.getOutlineThickness();
    output_data["isStatic"] = isStatic;
    output_data["mass"] = mass;
    output_data["type"] = getObjectType();
}


ObjectType::ID ImportableCircle::getObjectType() const
{
    return ObjectType::SimpleCircle;
}

sf::FloatRect ImportableCircle::getBoundingRect() const
{
    return getTransform().transformRect(circle.getGlobalBounds());
}

void ImportableCircle::setRadius(float radius)
{
    circle.setRadius(radius);
}


void ImportableCircle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(circle, states);
}
