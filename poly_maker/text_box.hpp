#ifndef TEXT_BOX_HPP
#define TEXT_BOX_HPP

#include "component.hpp"
#include "button.hpp"

namespace myGUI
{
class TextBox : public Component
{
public:
    enum Way
    {
        Down, Right
    };

    TextBox(const sf::RenderWindow& contextWindow, const std::string& title, const sf::Texture& buttonTexture, const sf::Font& textFont, unsigned int characterLength = 10);
    virtual ~TextBox();

    void update(sf::Time dt) override;
    void handleEvent(const sf::Event& e) override;

    void click() override;
    void hover(bool flag) override;

    //get the inside text of the text box number 'index' ranging from 0 to the number of textboxes added - 1
    //if the index exceeds this range, there will be unknown behavior happening
    std::string getInsideText(unsigned int index) const;
    ComponentType getType() const;
		

    //adds a text box input to the vector of text boxes and returns the index of it (see getInsideText)
    //the which_way argument is to say in which direction do you want the text box to be added,
    //it can either be to the right or downwards.
    unsigned int addTextBox(const std::string& title, unsigned int characterLength = 10U, Way which_way = Right);

    unsigned int getNumberOfTextBoxes() const;

    sf::FloatRect getBoundingRect() const override;

    void setSubmitButtonCallback(const std::function<void()>& callback);
    void setSubmitButtonText(const std::string& button_text);

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    class TextBoxInput : public sf::Drawable, public sf::Transformable
    {
        void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    public:
        TextBoxInput()
            :textBox(), insideText(), titleText() {}
        TextBoxInput(const std::string& title, const sf::Font& font, unsigned int characterLength = 10);
        
        sf::FloatRect getBoundingRect() const;

        sf::RectangleShape textBox;
        sf::Text insideText;
        sf::Text titleText;
        unsigned int maxCharacterLength = 0;
        unsigned int currentCharacterLength = 0;
    };

    std::vector<TextBoxInput> textInputs;   //the text box itself, can have multiple text boxes
    TextBoxInput* currentTextBoxInput;  //the text box that is currently active (the user writes in it)
    unsigned int currentTextBoxIndex = 0;
    sf::Vector2f maxTextBoxSize; //the maximum size of all the textboxes that are in textInputs (used by getBoundingRect)
    void handleCurrentTextBoxEvent(const sf::Event& e);
    bool isMouseInTextBox();
    void setTextBoxesColor(sf::Color color);

    const sf::RenderWindow& contextWindow;

    Button submitButton;

    bool isClickedOn = false;

};

}


#endif //TEXT_BOX_HPP