#include "importable_physical_sprite.hpp"
#include "text_box.hpp"
#include "menu.hpp"

namespace
{
    enum class textBoxesOptions
    {
        isStatic, mass
    };
}

ImportablePhysicalSprite::ImportablePhysicalSprite(TextureHolder& textures_)
    :ImportableObject(), sprite(), textures(textures_)
{
}

ImportablePhysicalSprite::~ImportablePhysicalSprite()
{
}

ImportableObject *ImportablePhysicalSprite::cloneImportableObject()
{
    return new ImportablePhysicalSprite(*this);
}

ImportableObject *ImportablePhysicalSprite::initImportableObjectFromJSON(const nlohmann::json &object_data)
{
    isStatic = object_data["isStatic"];
    mass = object_data["mass"];
    textureFileName = object_data["textureFileName"];

    sf::Texture* texture = nullptr;
    if(!(texture = textures.isResourceFileAlreadyLoaded(TEXTURE_DIR + textureFileName)))
    {
        size_t textures_num = textures.getNumberOfResourceLoaded() + Textures::TexturesCount;
        textures.load(TEXTURE_DIR + textureFileName, (Textures::ID)textures_num);
        texture = &textures.get((Textures::ID)textures_num);
    }

    sprite.setTexture(*texture);
    sprite.setTextureRect(sf::IntRect(object_data["textureRectangle"][0], object_data["textureRectangle"][1]
                                        , object_data["textureRectangle"][2], object_data["textureRectangle"][3]));

    return this;
}

myGUI::TextBox *ImportablePhysicalSprite::createImportableObjectMenuOptions(const sf::RenderWindow &contextWindow, const TextureHolder &_textures, const FontHolder &fonts)
{
    myGUI::TextBox* spriteTextBoxes = new myGUI::TextBox(contextWindow, "isStatic", _textures.get(Textures::Button), fonts.get(Fonts::Stats), 2U);
    spriteTextBoxes->addTextBox("mass", 5U);
    spriteTextBoxes->setSubmitButtonText("apply");
    spriteTextBoxes->setSubmitButtonCallback(
        [this, spriteTextBoxes]()
        {
            std::string static_text = spriteTextBoxes->getInsideText((unsigned int)textBoxesOptions::isStatic);
            if(static_text != "")
                isStatic = (static_text[0] == 'y') ? true : false;

            float mass_buff = mass;
            mass = sscanf(spriteTextBoxes->getInsideText((unsigned int)textBoxesOptions::mass).c_str(), "%f", &mass) == EOF ? mass_buff : mass;

            if(spriteTextBoxes->parentMenu)
                spriteTextBoxes->parentMenu->isMenuClosed = true;
        }
    );

    return spriteTextBoxes;
}

void ImportablePhysicalSprite::exportObjectData(nlohmann::json &output_data)
{
    output_data["mass"] = mass;
    output_data["isStatic"] = isStatic;
    output_data["textureFileName"] = textureFileName;
    sf::IntRect texRect = sprite.getTextureRect();
    output_data["textureRectangle"] = {texRect.left, texRect.top, texRect.width, texRect.height};
    output_data["type"] = getObjectType();
}

ObjectType::ID ImportablePhysicalSprite::getObjectType() const
{
    return ObjectType::SimpleSprite;
}

sf::FloatRect ImportablePhysicalSprite::getBoundingRect() const
{
    return getTransform().transformRect(sprite.getGlobalBounds());
}

void ImportablePhysicalSprite::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(sprite, states);
}
