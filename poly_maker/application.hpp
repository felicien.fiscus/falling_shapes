#ifndef APPLICATION_HPP
#define APPLICATION_HPP
#include "../resource_identifiers.hpp"
#include "menu.hpp"
#include "poly_builder.hpp"
#include "circle_builder.hpp"

namespace myGUI
{
    class Button;
    class TextBox;
}

class Application
{

public:
    Application();
    ~Application();

    void run();


private:

    enum State
    {
        None, BuildPolygon, BuildCircle
    };
    State currentState;

    sf::RenderWindow fWindow;
    sf::Time fFrameRate;
    
    sf::Text fRealFrameRateText;
    
    FontHolder fFonts;
    TextureHolder fTextures;
    void loadFonts();
    void loadTextures();

    sf::RectangleShape menuBackground;
    myGUI::Menu fMenu;
    unsigned int numberOfSpriteImported = Textures::ID::TexturesCount;

    void buildMenu();
    void buildCircleBuilderButton();
    void buildPolyBuilderButton();
    void buildExportButton();
    void buildImportButton();
    void buildImportSpriteTextBox();
    
    void importAccordingToType(ObjectType::ID type, myGUI::TextBox* importSpriteTextBox, const sf::IntRect& texrect);
    void importSimplePhysicalObjectSprite(myGUI::TextBox* importSpriteButton, const sf::IntRect& textureRect);
    
    std::list<ImportableObject*> sprites;
    ImportableObject* currentSpriteAttached = nullptr;
    ImportableObject* doesSpriteContainsPoint(const sf::Vector2f& point);
    void handleNoneStateEvents(const sf::Event& e);
    void updateNoneState(sf::Time dt);
    void exportSprites(const std::string& file);

    myGUI::Button* circleBuilderButtonRef = nullptr;
    myGUI::Button* polyBuilderButtonRef = nullptr;

    PolyBuilder polyBuilder;
    CircleBuilder circleBuilder;
    bool shiftPressed = false;

    void update();
    void handleEvent();
    void render();

    void keyEvents(const sf::Event& event);

    void importShapes(const std::string& input_file);
    void importCircle(const nlohmann::json& shape_data);
    void importPolygon(const nlohmann::json& shape_data);

    void moveEverythingBy(const sf::Vector2f& offset);

};

#endif //APPLICATION_HPP