#ifndef IMPORTABLE_OBJECT_HPP
#define IMPORTABLE_OBJECT_HPP
#include "../resource_identifiers.hpp"
#include "../json.hpp"
//if x == y, then return z
#define ifret(x, y, z) if(x == y) return z;


class ColorConverter
{
public:
    static sf::Color fromStringToColor(const std::string& color)
    {
        ifret(color,"black",sf::Color::Black)
        ifret(color,"blue",sf::Color::Blue)
        ifret(color,"cyan",sf::Color::Cyan)
        ifret(color,"green",sf::Color::Green)
        ifret(color,"magenta",sf::Color::Magenta)
        ifret(color,"red",sf::Color::Red)
        ifret(color,"white",sf::Color::White)
        ifret(color,"yellow",sf::Color::Yellow)

        return sf::Color::Transparent;
    }

    //rgb_text is supposed to be like this "r g b a"
    static void tryInitColorWithRGB(sf::Color& color, const std::string& rgb_text)
    {
        int r, g, b, a;
        color = (sscanf(rgb_text.c_str(), "%i %i %i %i", &r, &g, &b, &a) == EOF) ? color : sf::Color(r, g, b, a);
    }
};

class PhysicalObject;
namespace myGUI
{
    class TextBox;
}


// object that can be imported in polymaker and in the physics engine
class ImportableObject : public sf::Drawable, public sf::Transformable
{
public:
    ImportableObject(){}
    virtual ~ImportableObject(){}

    //get a copy of the 'this' instance of the object
    virtual ImportableObject* cloneImportableObject();

    //initializes the content of this object and returns a pointer to this
    virtual ImportableObject* initImportableObjectFromJSON(const nlohmann::json& object_data);
    
    virtual myGUI::TextBox* createImportableObjectMenuOptions(const sf::RenderWindow& contextWindow,
                                                             const TextureHolder& textures, 
                                                             const FontHolder& fonts);
    virtual void exportObjectData(nlohmann::json& output_data);

    virtual ObjectType::ID getObjectType() const;
    virtual sf::FloatRect getBoundingRect() const;

    static ImportableObject* getInstanceFromObjectType(ObjectType::ID type, TextureHolder& textures);

private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif //IMPORTABLE_OBJECT_HPP