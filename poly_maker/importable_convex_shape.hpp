#ifndef IMPORTABLE_CONVEX_SHAPE_HPP
#define IMPORTABLE_CONVEX_SHAPE_HPP
#include "importable_object.hpp"

class ImportableConvexShape : public ImportableObject
{
public:
    ImportableConvexShape();
    ~ImportableConvexShape() override;

    ImportableObject* cloneImportableObject() override;

    ImportableObject* initImportableObjectFromJSON(const nlohmann::json& object_data) override;
    
    myGUI::TextBox* createImportableObjectMenuOptions(const sf::RenderWindow& contextWindow,
                                                             const TextureHolder& textures, 
                                                             const FontHolder& fonts) override;

    void exportObjectData(nlohmann::json& output_data) override;
    
    ObjectType::ID getObjectType() const override;
    sf::FloatRect getBoundingRect() const override;

    void setPolygonPoints(const sf::VertexArray& points_array);
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    sf::ConvexShape shape;
    bool isStatic = false;
    float mass = 0.5f;
};



#endif //IMPORTABLE_CONVEX_SHAPE_HPP