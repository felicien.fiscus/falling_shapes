#ifndef MENU_H
#define MENU_H
#include "../resource_identifiers.hpp"
#include "component.hpp"


namespace myGUI
{
	class Component;
	/**
	a Menu is simply a collection of Components put together
	*/
	class Menu : public Component
	{
	public:
		Menu(const sf::RenderWindow& contextWindow);
		~Menu();

		void update(sf::Time dt) override;
		void handleEvent(const sf::Event& e) override;
		ComponentType getType() const;
		
		//comp can be allocated in the heap as the Menu destructor takes responsibility for destroying the components
		//The function not only adds the component to fComponents, but also moves it a little bit so that the components don't overlap each other
		//It moves them from the top to the bottom, from the left to the right
		void addComp(Component* comp);

		void removeComp(Component* comp);
		//remove all components
		void clearComponents();

		sf::FloatRect getBoundingRect() const;

		std::vector<Component*>::iterator begin();
		std::vector<Component*>::iterator end();

		bool isMenuClosed = true;
	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		
		std::vector<Component*> fComponents;
		const sf::RenderWindow& fContextWindow;
	};
}

#endif //MENU_H