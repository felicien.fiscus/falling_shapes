#ifndef IMPORTABLE_CIRCLE_HPP
#define IMPORTABLE_CIRCLE_HPP
#include "importable_object.hpp"

class ImportableCircle : public ImportableObject
{
public:
    ImportableCircle();
    ~ImportableCircle() override;

    //get a copy of the 'this' instance of the object
    ImportableObject* cloneImportableObject() override;

    //initializes the content of this object and returns a pointer to this
    ImportableObject* initImportableObjectFromJSON(const nlohmann::json& object_data) override;
    
    myGUI::TextBox* createImportableObjectMenuOptions(const sf::RenderWindow& contextWindow,
                                                             const TextureHolder& textures, 
                                                             const FontHolder& fonts) override;
    void exportObjectData(nlohmann::json& output_data) override;

    ObjectType::ID getObjectType() const override;
    sf::FloatRect getBoundingRect() const override;

    void setRadius(float radius);

    PhysicalObject* parentPhysicalBounds = nullptr;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    sf::CircleShape circle;
    bool isStatic = false;
    float mass = 0.5f;
};



#endif //IMPORTABLE_CIRCLE_HPP