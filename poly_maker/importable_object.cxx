#include "importable_object.hpp"
#include "all_importables_includes.hpp"

ImportableObject* ImportableObject::getInstanceFromObjectType(ObjectType::ID type, TextureHolder& textures)
{
    switch (type)
    {
    case ObjectType::None:
        return new ImportableObject();
        break;
    case ObjectType::SimpleCircle:
        return new ImportableCircle();
        break;
    case ObjectType::SimplePolygon:
        return new ImportableConvexShape();
        break;
    case ObjectType::SimpleSprite:
        return new ImportablePhysicalSprite(textures);
        break;
    
    default:
        break;
    }

    throw std::runtime_error("Unknown object type passed as argument in getInstanceFromObjecType function");
}

ImportableObject* ImportableObject::cloneImportableObject()
{
    return new ImportableObject();
}

ImportableObject* ImportableObject::initImportableObjectFromJSON(const nlohmann::json& object_data)
{
    return this;
}

myGUI::TextBox* ImportableObject::createImportableObjectMenuOptions(const sf::RenderWindow& contextWindow,
                                                            const TextureHolder& textures, 
                                                            const FontHolder& fonts)
{
    return nullptr;
}
void ImportableObject::exportObjectData(nlohmann::json& output_data)
{
}

ObjectType::ID ImportableObject::getObjectType() const
{
    return ObjectType::None;
}
sf::FloatRect ImportableObject::getBoundingRect() const
{
    return sf::FloatRect();
}

void ImportableObject::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    return;
}