#include "circle_builder.hpp"
#include "../../maths_operations.hpp"
#include <fstream>
#include "text_box.hpp"
#include "importable_circle.hpp"

CircleBuilder::CircleBuilder(const sf::RenderWindow& context_window, const TextureHolder& textures_, const FontHolder& fonts_)
    :contextWindow(context_window), circles(), mousePositionToCreateCircle(), previousMousePosition(), menuOptionsBackground(sf::Vector2f(WIDTH, HEIGHT))
    , objectMenuOptions(context_window), textures(textures_), fonts(fonts_)
{
    attachedCircle.importableObject = new ImportableCircle();
    menuOptionsBackground.setFillColor(sf::Color(0,0,0,120));
}


void CircleBuilder::buildFrontCircleOptionMenu()
{
    if(circles.size() == 0)
        return;
    using namespace myGUI;

    TextBox*  frontCircleTextBox = circles.front().importableObject->createImportableObjectMenuOptions(contextWindow, textures, fonts);
    if(!frontCircleTextBox)
    {
        objectMenuOptions.isMenuClosed = true;
        return;
    }
    frontCircleTextBox->setPosition( WIDTH * 0.5f - frontCircleTextBox->getBoundingRect().width * 0.5f,
        HEIGHT * 0.5f - frontCircleTextBox->getBoundingRect().height * 0.5f);
    
    objectMenuOptions.addComp(frontCircleTextBox);
}

CircleBuilder::~CircleBuilder()
{
    for(auto& circle : circles)
    {
        if(circle.circle)
            delete circle.circle;
        if(circle.importableObject)
            delete circle.importableObject;
    }

    if(attachedCircle.circle)
        delete attachedCircle.circle;
    if(attachedCircle.importableObject)
        delete attachedCircle.importableObject;
}

void CircleBuilder::update(sf::Time dt)
{
    if(!objectMenuOptions.isMenuClosed)
        objectMenuOptions.update(dt);
}


CircleBuilder::CirclePair CircleBuilder::findCircleContainingMouse() const
{
    for(auto& circle : circles)
    {
        if(circle.circle
         && Utility::length(sf::Vector2f(sf::Mouse::getPosition(contextWindow)) - circle.circle->getPosition()) <= circle.circle->getRadius())
        {
                return circle;
        }
        else if(circle.importableObject->getBoundingRect().contains(sf::Vector2f(sf::Mouse::getPosition(contextWindow))))
        {
            return circle;
        }
    }   

    return CirclePair();

}


void CircleBuilder::moveCircles(const sf::Vector2f& offset)
{
    for(auto& circle : circles)
    {
        if(circle.circle)
            circle.circle->move(offset);
        circle.importableObject->move(offset);
    }
}

void CircleBuilder::addCirclePair(const CirclePair& circle, bool isBeingConstructed)
{
    if(isBeingConstructed && !circle.circle)
    {
        circles.push_front(circle);
        return;
    }

    if(!circle.circle || isBeingConstructed)
        throw std::runtime_error("wrong initialization for parameter circle in addCirclePair");
        
    circles.push_front(circle);
}

void CircleBuilder::createCircle()
{
    buttonPressed = true;
    if(attachedCircle.circle)  //then we do not create but rather detach the circle attached to the cursor
    {
        circles.push_front(attachedCircle);
        attachedCircle.circle = nullptr;
        attachedCircle.importableObject = new ImportableCircle();
    }
    else
    {
        mousePositionToCreateCircle = sf::Vector2f(sf::Mouse::getPosition(contextWindow));
        sf::CircleShape* new_circle = new sf::CircleShape(0.f);
    
        new_circle->setPosition(mousePositionToCreateCircle);
        new_circle->setFillColor(sf::Color::Transparent);
        new_circle->setOutlineThickness(1.f);
        new_circle->setOutlineColor(sf::Color::Blue);

        if(circles.size() > 0 && !circles.front().circle)   //then that means we've only deleted the circle bound but the circle sprite is still there
        {
            circles.front().circle = new_circle;
        }
        else
        {
            CirclePair new_circlepair;
            new_circlepair.circle = new_circle;
            new_circlepair.importableObject = new ImportableCircle();
            circles.push_front(new_circlepair);
        }
        currentColor = sf::Color::Blue;
    }
}
void CircleBuilder::expandCircle(float amount)
{
    circles.front().circle->setRadius(amount);
    circles.front().circle->setOrigin(amount, amount);
}
void CircleBuilder::deleteCircle(CircleBuilder::CirclePair circle)
{

    if(controlKeyPressed || !circle.importableObject || circle.importableObject->getObjectType() == ObjectType::None)
    {
        circles.remove(circle);
        if(circle.importableObject)
            delete circle.importableObject;
        if(circle.circle)
            delete circle.circle;
        return;
    }

    //then we must make this circle the new front circle (so that the user can attach a new circle bound to it)
    if(circle.circle)
    {
        circles.remove(circle);//first remove the element
        delete circle.circle;
        circle.circle = nullptr;
        //and then put it back to the front of the list
        circles.push_front(circle);
    }
}


void CircleBuilder::copyCircle(const CirclePair& circle)
{
    if(circle.circle)
    {
        if(attachedCircle.circle)
        {
            delete attachedCircle.circle;
            attachedCircle.circle = nullptr;
        }

        attachedCircle.circle = new sf::CircleShape(*circle.circle);
        if(attachedCircle.importableObject)
            delete attachedCircle.importableObject;
        attachedCircle.importableObject = circle.importableObject->cloneImportableObject();
        previousMousePosition = sf::Vector2f(sf::Mouse::getPosition(contextWindow));
    }
}

void CircleBuilder::handleEvent(const sf::Event& e)
{
    sf::Vector2f mousePosition = sf::Vector2f(sf::Mouse::getPosition(contextWindow));
    if(mousePosition.x < 0.f || mousePosition.x > WIDTH 
        || mousePosition.y < 0.f || mousePosition.y > HEIGHT)
            return;
        
    if(!objectMenuOptions.isMenuClosed)
    {
        objectMenuOptions.handleEvent(e);
        if(e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::Escape)
        {
            objectMenuOptions.isMenuClosed = true;
            objectMenuOptions.clearComponents();
        }
        return;
    }

    switch (e.type)
    {
    case sf::Event::KeyPressed:
        switch (e.key.code)
        {
        case sf::Keyboard::D:   //Duplicate the circle we have the cursor in and grab it
        {
            CirclePair circle_found = findCircleContainingMouse();
            copyCircle(circle_found);
            
        }
            break;
        case sf::Keyboard::M: //move the circle we have the cursor in, because I already had the copying stuff to work I was just lazy and reused it
        {
            CirclePair circle_found = findCircleContainingMouse();
            copyCircle(circle_found);
            //and then delete the original
            if(circle_found.circle)
            {
                circles.remove(circle_found);
                delete circle_found.circle;
            }
        }
        break;
        case sf::Keyboard::LControl:
            controlKeyPressed = true;
            break;
        default:
            break;
        }

        
    break;
    case sf::Event::KeyReleased:
        if(e.key.code == sf::Keyboard::LControl)
            controlKeyPressed = false;
    break;
    case sf::Event::MouseButtonPressed:
        if(e.mouseButton.button == sf::Mouse::Right)
        {
            //then erase the circle we're on (if we are on one)
            CirclePair circle_found = findCircleContainingMouse();
            deleteCircle(circle_found);
            
        }
        else
        {
            createCircle();
        }
    break;
    case sf::Event::MouseButtonReleased:
        buttonPressed = false;
        if(e.mouseButton.button == sf::Mouse::Left)
        {
            if(circles.size() > 0 && circles.front().circle)
            {
                if(circles.front().circle->getRadius() <= 2.f)
                    deleteCircle(circles.front());
                else if(!attachedCircle.circle)
                {
                    objectMenuOptions.isMenuClosed = false;
                    if(circles.front().importableObject->getObjectType() == ObjectType::SimpleCircle)
                    {   //then we need to set the radius of the simple circle importable object
                        float radius = circles.front().circle->getRadius();
                        ImportableCircle* c = static_cast<ImportableCircle*>(circles.front().importableObject);
                        c->setRadius(radius);
                        c->setPosition(circles.front().circle->getPosition() - sf::Vector2f(radius, radius));
                    }
                    objectMenuOptions.clearComponents();
                    buildFrontCircleOptionMenu();
                }
            }
        }
    break;
    case sf::Event::MouseMoved:
        if(attachedCircle.circle)
        {
            sf::Vector2f currentMousePosition(sf::Mouse::getPosition(contextWindow));
            attachedCircle.circle->move(currentMousePosition - previousMousePosition);
            attachedCircle.importableObject->move(currentMousePosition - previousMousePosition);
            previousMousePosition = currentMousePosition;
        }
        else if(buttonPressed && circles.front().circle)
        {
            float mouseDelta = Utility::length(sf::Vector2f(sf::Mouse::getPosition(contextWindow)) - mousePositionToCreateCircle);
            expandCircle(mouseDelta);
        }
    break;
    default:
        break;
    }
}

bool CircleBuilder::isShowingMenu() const
{
    return !objectMenuOptions.isMenuClosed;
}

void CircleBuilder::exportCircles(const std::string& outputFile)
{
    nlohmann::json polyJSON;
    //first get the potential json already in the file
    std::ifstream in_file{outputFile};
    if(in_file.is_open())
    {
        in_file >> polyJSON;
    }

    for(auto& circle : circles)
    {
        if(!circle.circle)
            continue;
        polyJSON.push_back(nlohmann::json());
        nlohmann::json& lastAdded = polyJSON[polyJSON.size() - 1];

        lastAdded["boundsType"] = "circle";
        lastAdded["radius"] = circle.circle->getRadius();
        lastAdded["boundsPosition"] = {circle.circle->getPosition().x, circle.circle->getPosition().y};
        //we do this because physical shapes will be linked to their bounds, and so their position will be
        //according to the top left corner of the physical bounds, hence the minus (position - origin)
        //note that we only need to do that for the circle builder as 
        //the polybuilder doesn't have any origin or transform, it's just a set of points (i.e a vertex array)
        sf::Vector2f relativePosition = circle.importableObject->getPosition() - circle.circle->getPosition() + circle.circle->getOrigin();
        lastAdded["position"] = {relativePosition.x, relativePosition.y};
        lastAdded["rotation"] = circle.importableObject->getRotation();
        lastAdded["scale"] = {circle.importableObject->getScale().x, circle.importableObject->getScale().y};
        lastAdded["origin"] = {circle.importableObject->getOrigin().x, circle.importableObject->getOrigin().y};
        circle.importableObject->exportObjectData(lastAdded);

    }

    //then dump it in a json file
    std::ofstream out_file{outputFile};
    if(!out_file.is_open())
        throw std::runtime_error("unable to open file: " + outputFile);

    out_file << std::setw(4) << polyJSON << "\n]" << std::endl;
    
    out_file.close();
}
void CircleBuilder::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    //first draw the objects associated with the circles
    for(auto& circle : circles)
    {
            target.draw(*circle.importableObject);
    }
    
    target.draw(*attachedCircle.importableObject);

    if(circles.size() > 0 && !circles.front().circle)
    {
        sf::RectangleShape glowingRect(sf::Vector2f(circles.front().importableObject->getBoundingRect().width, circles.front().importableObject->getBoundingRect().height));
        glowingRect.setPosition(circles.front().importableObject->getPosition());
        glowingRect.setFillColor(sf::Color(155, 155, 155, 80));
        glowingRect.setOutlineColor(sf::Color::Transparent);
        glowingRect.setOutlineThickness(2.f);

        target.draw(glowingRect);
        
    }

    //and then draw the surrounding circles
    for(auto& circle : circles)
        if(circle.circle)
            target.draw(*circle.circle);

    if(attachedCircle.circle)
        target.draw(*attachedCircle.circle);

    if(!objectMenuOptions.isMenuClosed)
    {
        target.draw(menuOptionsBackground);
        target.draw(objectMenuOptions);
    }
}
