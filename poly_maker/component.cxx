#include "component.hpp"
#include "menu.hpp"

namespace myGUI
{

Component::Component(std::function<void()> callback)
	:fIsHovering(false), fCallback(callback)
{

}

Component::~Component()
{
}

sf::Vector2f Component::getAbsolutePosition() const
{
	if(!parentMenu)
		return getPosition();
	
	return parentMenu->getTransform().transformPoint(getPosition());
}

void Component::click()
{
	fCallback();
}

void Component::hover(bool flag)
{
	fIsHovering = flag;
}

bool Component::isHovering() const
{
	return fIsHovering;
}

}