#ifndef IMPORTABLE_PHYSICAL_SPRITE_HPP
#define IMPORTABLE_PHYSICAL_SPRITE_HPP
#include "importable_object.hpp"

class ImportablePhysicalSprite : public ImportableObject
{
public:
    ImportablePhysicalSprite(TextureHolder& textures_);
    ~ImportablePhysicalSprite() override;

    //get a copy of the 'this' instance of the object
    virtual ImportableObject* cloneImportableObject() override;

    //initializes the content of this object and returns a pointer to this
    virtual ImportableObject* initImportableObjectFromJSON(const nlohmann::json& object_data) override;
    
    virtual myGUI::TextBox* createImportableObjectMenuOptions(const sf::RenderWindow& contextWindow,
                                                             const TextureHolder& _textures, 
                                                             const FontHolder& fonts) override;
    virtual void exportObjectData(nlohmann::json& output_data) override;

    virtual ObjectType::ID getObjectType() const override;
    virtual sf::FloatRect getBoundingRect() const override;

    bool isStatic = false;
    float mass = 0.5f;
    sf::Sprite sprite;
    std::string textureFileName;
protected:
    TextureHolder& textures;
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;


};

#endif //IMPORTABLE_PHYSICAL_SPRITE_HPP