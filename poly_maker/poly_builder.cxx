#include "poly_builder.hpp"
#include <assert.h>
#include <iostream>
#include <fstream>
#include "../../maths_operations.hpp"
#include "text_box.hpp"
#include "importable_convex_shape.hpp"

#define completePolygonRadiusAcceptance 20.f

PolyBuilder::PolyBuilder(const sf::RenderWindow& context_window, const TextureHolder& textures_, const FontHolder& fonts_)
    :contextWindow(context_window), polygons(), currentLinePlacement(sf::PrimitiveType::Lines), polygonInConstruction(),
    attachedPolygon(), previousMousePosition(), menuOptionsBackground(sf::Vector2f(WIDTH, HEIGHT)), frontPolyMenuOptions(context_window),
    textures(textures_), fonts(fonts_)
{
    polygonInConstruction.importableObject = new ImportableConvexShape();
    currentLinePlacement.append(sf::Vertex(sf::Vector2f(), sf::Color::Blue));
    currentLinePlacement.append(sf::Vertex(sf::Vector2f(), sf::Color::Blue));

    menuOptionsBackground.setFillColor(sf::Color(0,0,0, 120));
}

void PolyBuilder::buildFrontPolyMenuOptions()
{
    if(polygons.size() == 0)
        return;
    using namespace myGUI;
    
    TextBox* frontPolyTextBox = polygons.front().importableObject->createImportableObjectMenuOptions(contextWindow, textures, fonts);
    if(!frontPolyTextBox)
    {
        frontPolyMenuOptions.isMenuClosed = true;
        return;
    }
    
    frontPolyTextBox->setPosition( WIDTH * 0.5f - frontPolyTextBox->getBoundingRect().width * 0.5f,
        HEIGHT * 0.5f - frontPolyTextBox->getBoundingRect().height * 0.5f);
    
    frontPolyMenuOptions.addComp(frontPolyTextBox);
}

bool PolyBuilder::isShowingMenu() const
{
    return !frontPolyMenuOptions.isMenuClosed;
}

PolyBuilder::~PolyBuilder()
{
    for(auto& poly : polygons)
    {
        if(poly.poly)
            delete poly.poly;
    }
    if(polygonInConstruction.poly)
        delete polygonInConstruction.poly;
    if(attachedPolygon.poly)
        delete attachedPolygon.poly;
}

bool PolyBuilder::doesPolygonContainsPoint(const sf::Vector2f& point, const sf::VertexArray& poly)
{

    float sin_sign = 1.f;
    if(Utility::crossProduct2D(poly[0].position - poly[1].position, poly[2].position - poly[1].position) <= 0.f)
        sin_sign = -1.f;

    //the points are oredered in an anticlockwise manner so if the point is on the right side of any edge, it means it's outside, otherwise it's inside
    for(unsigned int i = 0;i<poly.getVertexCount() - 1;++i)
    {
        sf::Vector2f p1 = poly[i].position;
        sf::Vector2f p2 = poly[i+1].position;
        float cp = Utility::crossProduct2D(point - p1, p2 - p1);
        if(sin_sign * cp <= 0.f) //it's on the other side (sin <= 0.f or sin >= 0.f if clockwise)
            return false;
    }

    return true;
}

void PolyBuilder::update(sf::Time dt)
{
    if(!frontPolyMenuOptions.isMenuClosed)
    {
        frontPolyMenuOptions.update(dt);
        return;
    }

    if(!firstPoint)
        currentLinePlacement[1].position = sf::Vector2f(sf::Mouse::getPosition(contextWindow));
    
}

bool doNotIntersect(const sf::Vector2f& edge_beginPoint, const sf::Vector2f& edge_endPoint, const sf::VertexArray& points)
{
    sf::Vector2f edge = edge_endPoint - edge_beginPoint;
    for(unsigned int i = 0;i<points.getVertexCount() - 1;++i)
    {
        sf::Vector2f second_edge = points[i+1].position - points[i].position;
        float cp1 = Utility::crossProduct2D(points[i+1].position - edge_beginPoint, edge)
                * Utility::crossProduct2D(points[i].position - edge_beginPoint, edge);
        float cp2 = Utility::crossProduct2D(edge_endPoint - points[i].position, second_edge)
                * Utility::crossProduct2D(edge_beginPoint - points[i].position, second_edge);
        if(cp1 < 0.f && cp2 < 0.f)
            return false;
    }

    return true;
}

void PolyBuilder::setColorLatestPoly(sf::Color color)
{
    if(!polygonInConstruction.poly)
        return;

    for(unsigned int i = 0; i<polygonInConstruction.poly->getVertexCount();++i)
    {
        (*polygonInConstruction.poly)[i].color = color;
    }
}


void PolyBuilder::addVertexToCurrentPolygon()
{
    sf::Vector2f mousePosition(sf::Mouse::getPosition(contextWindow));
    if(firstPoint)
    {
        assert(!polygonInConstruction.poly);
        polygonInConstruction.poly = new sf::VertexArray(sf::PrimitiveType::LineStrip);
        polygonInConstruction.poly->append(sf::Vertex(mousePosition, currentColor));
        if(!polygonInConstruction.importableObject)
            polygonInConstruction.importableObject = new ImportableConvexShape();
        firstPoint = false;
    }
    else
    {
        assert(polygonInConstruction.poly);
        if(polygonInConstruction.poly->getVertexCount() <= 1)
            polygonInConstruction.poly->append(sf::Vertex(mousePosition, currentColor));
        else if(polygonInConstruction.poly->getVertexCount() == 2)   
        {
            sf::Vector2f p_minus1 = (*polygonInConstruction.poly)[0].position;
            sf::Vector2f p_minus2 = (*polygonInConstruction.poly)[1].position;
            polygonInConstruction.poly->append(sf::Vertex(mousePosition, currentColor));
            //check which way is the 'inside' of the polygon and which is the 'outside'
            if(Utility::crossProduct2D(p_minus2 - p_minus1, mousePosition - p_minus1) < 0.f)
                insideOutsideSign = -1; //the inside is downwards
            else
                insideOutsideSign = 1;  //the inside is upwards
        }
        else //add only if the convex property is respected and if edges do not intersect each other
        {
            sf::Vector2f p_minus1 = (*polygonInConstruction.poly)[polygonInConstruction.poly->getVertexCount() - 1].position;
            sf::Vector2f p_minus2 = (*polygonInConstruction.poly)[polygonInConstruction.poly->getVertexCount() - 2].position;

            //check if the next point we're about to place holds the convex property of the polygon
            float sinOfAngle = (insideOutsideSign > 0) ? Utility::crossProduct2D(p_minus2 - p_minus1, mousePosition - p_minus1)
                                : Utility::crossProduct2D(mousePosition - p_minus1, p_minus2 - p_minus1);

            if(sinOfAngle > 0.f)    //then the outside angle is less than 180° so it's not convex !
                return;
            if(!doNotIntersect(p_minus1, mousePosition, *polygonInConstruction.poly))    //then the new edge intersect some other edge
                return;
            
            if(Utility::length(mousePosition - (*polygonInConstruction.poly)[0].position) <= completePolygonRadiusAcceptance)    //the polygon is built
            {
                p_minus2 = mousePosition;
                mousePosition = (*polygonInConstruction.poly)[1].position;
                p_minus1 = (*polygonInConstruction.poly)[polygonInConstruction.poly->getVertexCount() - 1].position;
                sinOfAngle = (insideOutsideSign > 0) ? Utility::crossProduct2D(p_minus2 - p_minus1, mousePosition - p_minus1)
                                : Utility::crossProduct2D(mousePosition - p_minus1, p_minus2 - p_minus1);
                if(sinOfAngle < 0.f)
                    return;

                polygonInConstruction.poly->append((*polygonInConstruction.poly)[0]);
                firstPoint = true;
                polygons.push_front(polygonInConstruction);
                polygonInConstruction.poly = nullptr;
                polygonInConstruction.importableObject = new ImportableConvexShape();
                currentColor = sf::Color::Blue;
                currentLinePlacement[0].position = currentLinePlacement[1].position = sf::Vector2f();
                frontPolyMenuOptions.isMenuClosed = false;
                if(polygons.front().importableObject->getObjectType() == ObjectType::SimplePolygon)
                {
                    ImportableConvexShape* shape = static_cast<ImportableConvexShape*>(polygons.front().importableObject);
                    shape->setPolygonPoints(*polygons.front().poly);
                }
                frontPolyMenuOptions.clearComponents();
                buildFrontPolyMenuOptions();
                return;
            }
            else
                polygonInConstruction.poly->append(sf::Vertex(mousePosition, currentColor));
        }
    }

    currentLinePlacement[0].position = (*polygonInConstruction.poly)[polygonInConstruction.poly->getVertexCount() - 1].position;
}

PolyBuilder::PolyPair PolyBuilder::getPolygonContainingCursor() const
{
    for(auto& poly : polygons)
    {
        if(doesPolygonContainsPoint(sf::Vector2f(sf::Mouse::getPosition(contextWindow)), *poly.poly))
        {
            return poly;
        }
    }

    return PolyPair();
}

void PolyBuilder::deleteCurrentPolygon()
{
    if(!polygonInConstruction.poly)
    {
        PolyPair poly = getPolygonContainingCursor();
        
        if(poly.poly)
        {
            if(isControlKeyPressed) //just remove it completely (with the sprite)
            {
                polygons.remove(poly);
                if(poly.importableObject)
                    delete poly.importableObject;
            }
            else    //then just remove the polygon but keep the sprite and make it the polygon in construction
            {
                polygonInConstruction = poly;
                polygons.remove(poly);
                polygonInConstruction.poly = nullptr;
            }

            delete poly.poly;
            poly.poly = nullptr;
        }

        return;
    }

    firstPoint = true;
    delete polygonInConstruction.poly;
    polygonInConstruction.poly = nullptr;
    currentLinePlacement[0].position = currentLinePlacement[1].position = sf::Vector2f();
    if(isControlKeyPressed && polygonInConstruction.importableObject)
    {
        delete polygonInConstruction.importableObject;
        polygonInConstruction.importableObject = new ImportableConvexShape();
    }
}

void PolyBuilder::copyPolygon(const PolyPair& poly)
{
    if(poly.poly)
    {
        attachedPolygon.poly = new sf::VertexArray(*poly.poly);
        attachedPolygon.importableObject = poly.importableObject->cloneImportableObject();
        
        for(unsigned int i = 0;i<poly.poly->getVertexCount();++i)
        {
            (*attachedPolygon.poly)[i].position = Utility::getRotateVector((*attachedPolygon.poly)[i].position - poly.importableObject->getPosition()
                , -poly.importableObject->getRotation() * M_PI / 180.f) + poly.importableObject->getPosition();
        }

        attachedPolygon.importableObject->setRotation(0.f);
        previousMousePosition = sf::Vector2f(sf::Mouse::getPosition(contextWindow));
    }
}

void turnPolygon(float degrees, PolyBuilder::PolyPair& poly, const sf::RenderWindow& contextWindow)
{
    sf::Vector2f originPoint(sf::Mouse::getPosition(contextWindow));
    if(poly.importableObject->getObjectType() != ObjectType::None)
    {
        poly.importableObject->setOrigin(poly.importableObject->getOrigin() + (originPoint - poly.importableObject->getPosition()));
        poly.importableObject->setPosition(originPoint);
    
        poly.importableObject->rotate(degrees);
    }
    
    for(unsigned int i = 0;i<poly.poly->getVertexCount();++i)
    {
        (*poly.poly)[i].position = Utility::getRotateVector((*poly.poly)[i].position - originPoint, degrees * M_PI / 180.f) + originPoint;
    }
}

void PolyBuilder::handleEvent(const sf::Event& e)
{
    
    sf::Vector2f mousePosition(sf::Mouse::getPosition(contextWindow));
    if(mousePosition.x < 0.f || mousePosition.x > WIDTH 
        || mousePosition.y < 0.f || mousePosition.y > HEIGHT)
            return;
        

    if(!frontPolyMenuOptions.isMenuClosed)
    {
        frontPolyMenuOptions.handleEvent(e);
        if(e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::Escape)
        {
            frontPolyMenuOptions.isMenuClosed = true;
            frontPolyMenuOptions.clearComponents();
        }
        return;
    }

    switch (e.type)
    {
    case sf::Event::KeyPressed:
        switch (e.key.code)
        {
        case sf::Keyboard::A:
            if(attachedPolygon.poly)
            {
                turnPolygon(5.f, attachedPolygon, contextWindow);
            }
            break;
        case sf::Keyboard::Z:
            if(attachedPolygon.poly)
            {
                turnPolygon(-5.f, attachedPolygon, contextWindow);
            }
            break;
        case sf::Keyboard::D:   //duplicate the polygon
        {
            PolyPair new_poly = getPolygonContainingCursor();
            copyPolygon(new_poly);
        }
        break;
        case sf::Keyboard::M:   //copy and delete the original (i.e move it)
        {
            PolyPair polygonToMove = getPolygonContainingCursor();
            copyPolygon(polygonToMove);
            if(polygonToMove.poly)
            {
                polygons.remove(polygonToMove);
                delete polygonToMove.poly;
            }
        }
        break;
        case sf::Keyboard::LControl:
            isControlKeyPressed = true;
        break;
        default:
            break;
        }

    break;
    case sf::Event::KeyReleased:
        if(e.key.code == sf::Keyboard::LControl)
            isControlKeyPressed = false;
    break;
    case sf::Event::MouseButtonPressed:
        if(e.mouseButton.button == sf::Mouse::Button::Left)
        {
            if(attachedPolygon.poly)
            {
                polygons.push_front(attachedPolygon);
                attachedPolygon.poly = nullptr;
                attachedPolygon.importableObject = new ImportableConvexShape();
                frontPolyMenuOptions.isMenuClosed = false;
                frontPolyMenuOptions.clearComponents();
                buildFrontPolyMenuOptions();
            }
            else
                addVertexToCurrentPolygon();
        }
        else if(e.mouseButton.button == sf::Mouse::Button::Right)
        {
            deleteCurrentPolygon();
            currentColor = sf::Color::Blue;
        }
    break;
    case sf::Event::MouseMoved:
        if(attachedPolygon.poly)
        {
            for(unsigned int v = 0;v<attachedPolygon.poly->getVertexCount();++v)
            {
                (*attachedPolygon.poly)[v].position += mousePosition - previousMousePosition;
            }
            attachedPolygon.importableObject->move(mousePosition - previousMousePosition);
        }
        previousMousePosition = sf::Vector2f(sf::Mouse::getPosition(contextWindow));
    break;
    default:
        break;
    }
}

void PolyBuilder::addPolyPair(const PolyPair& poly, bool isBeingConstructed)
{
    if(isBeingConstructed)
    {
        if(polygonInConstruction.poly)
            delete polygonInConstruction.poly;
        polygonInConstruction = poly;
        return;
    }

    if(!poly.poly)
        throw std::runtime_error("null pointer in addPolyPair");

    polygons.push_front(poly);
}

void PolyBuilder::movePolygons(const sf::Vector2f& offset)
{
    for(auto& poly : polygons)
    {
        for(unsigned int v = 0;v<poly.poly->getVertexCount();++v)
        {
            (*poly.poly)[v].position += offset;
        }
        poly.importableObject->move(offset);
    }
}

bool arePointsOrderedInTrigoDirection(const sf::VertexArray& points)
{
    if(points.getVertexCount() < 3)
        return false;
    
    sf::Vector2f p0 = points[0].position;
    sf::Vector2f p1 = points[1].position;
    sf::Vector2f p2 = points[2].position;

    float cp = Utility::crossProduct2D(p0 - p1, p2 - p1);
    
    return cp < 0.f;
}

void PolyBuilder::exportPolygons(const std::string& outputFile)
{
    //first serialize the json
    nlohmann::json polyJSON = nlohmann::json::array();
    for(auto& poly : polygons)
    {
        polyJSON.push_back(nlohmann::json());

        nlohmann::json& lastAdded = polyJSON[polyJSON.size() - 1];
        nlohmann::json pjson = nlohmann::json::array();
        //need to make sure the points are in trigonometric order
        if(arePointsOrderedInTrigoDirection(*poly.poly))   //then the inside is down so it means that the points are in trigonometric order 
        { 
            for(unsigned v = 0;v<poly.poly->getVertexCount() - 1;++v)
            {
                pjson.push_back({(*poly.poly)[v].position.x, (*poly.poly)[v].position.y});
            }
        }
        else    //then the inside is up, so it means that the points are in anti-trigo order so we need to flip the order
        {
            for(int v = poly.poly->getVertexCount() - 1;v > 0;--v)
            {
                pjson.push_back({(*poly.poly)[v].position.x, (*poly.poly)[v].position.y});
            }
        }

        lastAdded["boundsType"] = "polygon";
        lastAdded["poly_vertices"] = pjson;
        lastAdded["origin"] = {poly.importableObject->getOrigin().x, poly.importableObject->getOrigin().y};
        lastAdded["position"] = {poly.importableObject->getPosition().x, poly.importableObject->getPosition().y};
        lastAdded["scale"] = {poly.importableObject->getScale().y, poly.importableObject->getScale().y};
        lastAdded["rotation"] = poly.importableObject->getRotation();
        poly.importableObject->exportObjectData(lastAdded);
    }

    //then dump it in a json file
    std::ofstream out_file{outputFile, std::ios_base::trunc};
    if(!out_file.is_open())
        throw std::runtime_error("unable to open file: " + outputFile);

    out_file << std::setw(4) << polyJSON << std::endl;
    
    out_file.close();
}

void PolyBuilder::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    //first draw the sprites
    for(auto& poly : polygons)
    {
        target.draw(*poly.importableObject);
    }

    target.draw(*polygonInConstruction.importableObject);

    if(polygonInConstruction.importableObject->getObjectType() != ObjectType::None)
    {
        sf::RectangleShape glowingRect(sf::Vector2f(polygonInConstruction.importableObject->getBoundingRect().width, polygonInConstruction.importableObject->getBoundingRect().height));
        glowingRect.setPosition(polygonInConstruction.importableObject->getPosition());
        glowingRect.setFillColor(sf::Color(255, 255, 255, 80));
        glowingRect.setOutlineColor(sf::Color::Transparent);
        glowingRect.setOutlineThickness(2.f);

        target.draw(glowingRect);
        
    }

    if(attachedPolygon.poly)
    {
        target.draw(*attachedPolygon.importableObject);
    }

    //and then draw the polygons
    for(auto& poly : polygons)
    {
        target.draw(*poly.poly);
    }

    if(polygonInConstruction.poly)
        target.draw(*polygonInConstruction.poly);    

    if(attachedPolygon.poly)
    {
        target.draw(*attachedPolygon.poly);
    }


    target.draw(currentLinePlacement);

    if(!frontPolyMenuOptions.isMenuClosed)
    {
        target.draw(menuOptionsBackground);
        target.draw(frontPolyMenuOptions);
    }
}