#include "text_box.hpp"
#include "menu.hpp"

#define separation_offset 10.f

namespace myGUI
{

TextBox::TextBoxInput::TextBoxInput(const std::string& title, const sf::Font& font, unsigned int characterLength)
    :textBox(sf::Vector2f(characterLength * 15.f, 40.f)), insideText("", font), titleText(title, font), maxCharacterLength(characterLength)
{
    titleText.setFillColor(sf::Color::White);
    
    //put it under the titleText with a little offset
    textBox.setPosition(0.f, titleText.getCharacterSize() + separation_offset);
    textBox.setFillColor(sf::Color::White);
    textBox.setOutlineColor(sf::Color::Blue);
    textBox.setOutlineThickness(2.f);

    //put it a little bit inside the rectangle box
    insideText.setPosition(5.f, textBox.getPosition().y + 5.f);
    insideText.setFillColor(sf::Color::Black);
}

void TextBox::TextBoxInput::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(titleText, states);
    target.draw(textBox, states);
    target.draw(insideText, states);
}

sf::FloatRect TextBox::TextBoxInput::getBoundingRect() const
{
    sf::Vector2f width_height(std::max({textBox.getSize().x, insideText.getLocalBounds().width, titleText.getLocalBounds().width})
                            , textBox.getPosition().y + textBox.getSize().y);
    
    return sf::FloatRect(getPosition(), width_height);
}

TextBox::TextBox(const sf::RenderWindow& context_window, const std::string& title, const sf::Texture& buttonTexture, const sf::Font& textFont, unsigned int characterLength)
    :Component(), textInputs(1, TextBoxInput(title, textFont, characterLength)), currentTextBoxInput(nullptr),
     maxTextBoxSize(textInputs[0].getBoundingRect().width, textInputs[0].getBoundingRect().height)
    , contextWindow(context_window), 
    submitButton(buttonTexture, textFont, "Submit", false)
{   
    //put it under the text box with a little offset
    submitButton.setPosition(0.f, textInputs[0].textBox.getSize().y + submitButton.getBoundingRect().height * 0.5f);
}
TextBox::~TextBox()
{

}

void TextBox::update(sf::Time dt)
{
}

#define unicode_undescore 0x0000005F
#define unicode_dot 0x0000002E
#define unicode_UpperCase_letter_start 0x00000041
#define unicode_LowerCase_letter_start 0x00000061
#define unicode_number_start 0x00000030
#define alphabetSize 26
#define spaceCharacter 0x00000020
//checks if the character is either '.' '_' ' ' or a lower case letter [a-z]
//or an upper case letter [A-Z] or a number [0-9]
bool isValidCharacter(sf::Uint32 unicode_char)
{
    return unicode_char == unicode_undescore || unicode_char == unicode_dot 
        || (unicode_char >= unicode_UpperCase_letter_start && unicode_char <= unicode_UpperCase_letter_start + alphabetSize)
        || (unicode_char >= unicode_LowerCase_letter_start && unicode_char <= unicode_LowerCase_letter_start + alphabetSize)
        || (unicode_char >= unicode_number_start && unicode_char <= unicode_number_start + 9)
        || unicode_char == spaceCharacter;
}

void TextBox::handleCurrentTextBoxEvent(const sf::Event& e)
{
    if(isClickedOn && currentTextBoxInput && e.type == sf::Event::TextEntered)
    {
        if(e.text.unicode == 0x00000008)    //backspace has been pressed
        {
            if(currentTextBoxInput->insideText.getString().getSize() > 0U)
            {
                currentTextBoxInput->insideText.setString(currentTextBoxInput->insideText.getString().substring(0, currentTextBoxInput->insideText.getString().getSize() - 1));
                currentTextBoxInput->currentCharacterLength--;
            }
        }
        else if(currentTextBoxInput->currentCharacterLength < currentTextBoxInput->maxCharacterLength && isValidCharacter(e.text.unicode))
        {
            currentTextBoxInput->insideText.setString(currentTextBoxInput->insideText.getString() + sf::String(e.text.unicode));
            currentTextBoxInput->currentCharacterLength++;
        }
        else if(e.text.unicode == 0x0000000D)   //'enter' or 'return' has been pressed
        {
            isClickedOn = false;
            submitButton.fCallback();
        }
    }
}

void TextBox::handleEvent(const sf::Event& e)
{
    handleCurrentTextBoxEvent(e);

    if(e.type == sf::Event::MouseButtonPressed && !fIsHovering)
        isClickedOn = false;
    
    if(isClickedOn && e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::Tab)
    {
        setTextBoxesColor(sf::Color::Blue);
        currentTextBoxInput = &(textInputs[(++currentTextBoxIndex) % textInputs.size()]);
        currentTextBoxInput->textBox.setOutlineColor(sf::Color::Red);

    }

    submitButton.handleEvent(e);
}

bool TextBox::isMouseInTextBox()
{
    sf::Vector2f mousePos(sf::Mouse::getPosition(contextWindow));
    mousePos -= getAbsolutePosition();
    for(unsigned int index = 0;index < textInputs.size();++index)
    {
        if(textInputs[index].getBoundingRect().contains(mousePos))
        {
            currentTextBoxInput = &(textInputs[index]);
            currentTextBoxIndex = index;
            return true;
        }
    }

    currentTextBoxInput = nullptr;
    return false;
}

void TextBox::setTextBoxesColor(sf::Color color)
{
    for(auto& textbox : textInputs)
    {
        textbox.textBox.setOutlineColor(color);
    }
}

ComponentType TextBox::getType() const
{
	return ComponentType::TextBox;
}

void TextBox::click()
{
    Component::click();
    isClickedOn = true;
    setTextBoxesColor(sf::Color::Blue);
    if(isMouseInTextBox())
    {
        currentTextBoxInput->textBox.setOutlineColor(sf::Color::Red);
    }

    if(submitButton.isHovering())
        submitButton.click();
}
void TextBox::hover(bool flag)
{
    Component::hover(flag);
    
    sf::Vector2f localMousePos = sf::Vector2f(sf::Mouse::getPosition(contextWindow)) - getAbsolutePosition();
    submitButton.hover(submitButton.getBoundingRect().contains(localMousePos));
    if(isClickedOn)
        return;

    if(fIsHovering && isMouseInTextBox())
    {
        currentTextBoxInput->textBox.setOutlineColor(sf::Color::Green);
    }
    else
    {
        setTextBoxesColor(sf::Color::Blue);
    }

}

std::string TextBox::getInsideText(unsigned int index) const
{
    return textInputs[index].insideText.getString().toAnsiString();
}

unsigned int TextBox::addTextBox(const std::string& title, unsigned int characterLength, Way which_way)
{
    TextBoxInput new_box(title, *(textInputs[0].insideText.getFont()), characterLength);
    sf::FloatRect previousSize = textInputs[textInputs.size() - 1].getBoundingRect();

    switch (which_way)
    {
    case Way::Down:
        new_box.setPosition(previousSize.left, previousSize.top + previousSize.height + separation_offset);
        maxTextBoxSize.y += new_box.getBoundingRect().height + separation_offset;
        break;
    case Way::Right:
        new_box.setPosition(previousSize.left + previousSize.width + separation_offset, previousSize.top);
        maxTextBoxSize.x += new_box.getBoundingRect().width + separation_offset;
        break;
    default:
        break;
    }

    textInputs.push_back(new_box);
    currentTextBoxInput = nullptr;
    submitButton.setPosition(submitButton.getPosition().x, maxTextBoxSize.y + submitButton.getBoundingRect().height * 0.5f);
    return textInputs.size() - 1;
}

unsigned int TextBox::getNumberOfTextBoxes() const
{
    return textInputs.size();
}

sf::FloatRect TextBox::getBoundingRect() const
{
    sf::Vector2f width_height(std::max(maxTextBoxSize.x, submitButton.getBoundingRect().width), submitButton.getPosition().y + 
        submitButton.getBoundingRect().height);
    if(!parentMenu)
        return sf::FloatRect(getPosition(), width_height);

    return parentMenu->getTransform().transformRect(sf::FloatRect(getPosition(), width_height));
}

void TextBox::setSubmitButtonCallback(const std::function<void()>& callback)
{
    submitButton.fCallback = callback;
}
void TextBox::setSubmitButtonText(const std::string& button_text)
{
    submitButton.setText(button_text);
}

void TextBox::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    for(auto& textbox : textInputs)
        target.draw(textbox, states);
    target.draw(submitButton, states);

/*     sf::FloatRect box = getBoundingRect();
    sf::RectangleShape rect(sf::Vector2f(box.width, box.height));
    rect.setPosition(box.left, box.top);
    rect.setFillColor(sf::Color::Transparent);
    rect.setOutlineColor(sf::Color::Red);
    rect.setOutlineThickness(2.f);

    target.draw(rect); */
}


}